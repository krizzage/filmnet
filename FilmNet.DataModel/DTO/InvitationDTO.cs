﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilmNet.DataModel.DTO
{
    public class InvitationDTO
    {
        public int UserId { get; set; }
        public int InviterId { get; set; }
        public virtual UserDTO Inviter { get; set; }

        public bool? IsAccepted { get; set; }
    }
}
