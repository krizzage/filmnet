﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilmNet.DataModel.DTO
{
    public class ActorRoleDTO
    {
        public int FilmId { get; set; }
        public int ActorId { get; set; }
        public string Name { get; set; }

        public FilmDTO Film { get; set; }
    }
}
