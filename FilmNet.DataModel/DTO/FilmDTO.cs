﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilmNet.DataModel.DTO
{
    public class FilmDTO
    {
        public FilmDTO()
        {
            Roles = new List<FilmRoleDTO>();
        }

        public int FilmId { get; set; }
        public int GenreId { get; set; }
        public string Name { get; set; }
        public double Average { get; set; }
        public string Description { get; set; }
        public int Year { get; set; }


        public GenreDTO Genre { get; set; }
        public List<FilmRoleDTO> Roles { get; set; }
        public List<RatingDTO> Ratings { get; set; }
    }
}
