﻿using FilmNet.Entities.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilmNet.DataModel.DTO
{
    public class ActorDTO
    {
        public ActorDTO()
        {
            Roles = new List<ActorRoleDTO>();
        }

        public int ActorId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Description { get; set; }

        public List<ActorRoleDTO> Roles { get; set; }
    }
}
