﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilmNet.DataModel.DTO
{
    public class RatingDTO
    {
        public int UserId { get; set; }
        public int FilmId { get; set; }
        public int UserRating { get; set; }
        public string Comment { get; set; }

        public UserDTO User { get; set; }
        public FilmDTO Film { get; set; }
    }
}
