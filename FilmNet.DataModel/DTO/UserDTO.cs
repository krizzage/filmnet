﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilmNet.DataModel.DTO
{
    public class UserDTO
    {
        public UserDTO()
        {

        }

        public int UserId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public bool SuperUser { get; set; }
    }
}
