﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilmNet.DataModel.DTO
{
    public class GenreDTO
    {
        public GenreDTO()
        {

        }

        public int GenreId { get; set; }
        public string Name { get; set; }
    }
}
