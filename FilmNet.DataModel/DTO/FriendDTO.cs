﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilmNet.DataModel.DTO
{
    public class FriendDTO
    {
        public int UserId { get; set; }
        public int UserFriendId { get; set; }
        public UserDTO UserFriend { get; set; }
        public DateTime Date { get; set; }
    }
}
