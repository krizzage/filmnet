﻿using FilmNet.DataModel.Abstract;
using FilmNet.Entities.Classes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilmNet.DataModel.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(FilmNetContext context) : base(context)
        {
        }

        public void AddInvitation(User from, User to)
        {
            context.Invitations.Add(new Invitation() { UserId = to.UserId, InviterId = from.UserId });
        }

        public void AddFriend(int firstId, int secondId)
        {
            context.Friends.Add(new Friend() { UserId = firstId, UserFriendId = secondId, Date = DateTime.Now });
            context.Friends.Add(new Friend() { UserId = secondId, UserFriendId = firstId, Date = DateTime.Now });
        }

        public IEnumerable<Friend> GetFriends(User user)
        {
            return context.Friends.Include("User").Include("UserFriend").Where(x => x.UserId == user.UserId);
        }

        public IEnumerable<Invitation> GetPendingInvitations(User user)
        {
            IQueryable<Invitation> query = context.Invitations;
            query = query.Include("User").Where(x => x.UserId == user.UserId && x.IsAccepted == null);

            return query.ToList();
        }

        public void AcceptInvitation(Invitation invitation)
        {
            var inv = context.Invitations.Single(x => x.UserId == invitation.UserId && x.InviterId == invitation.InviterId);
            inv.IsAccepted = true;
            this.AddFriend(inv.InviterId, inv.UserId);
        }

        public Invitation GetInvitationByUsersId(int userFrom, int userTo)
        {
            return context.Invitations.SingleOrDefault(x => x.UserId == userFrom && x.InviterId == userTo);
        }

        public void DeclineInvitation(Invitation invitation)
        {
            var inv = context.Invitations.Single(x => x.UserId == invitation.UserId && x.InviterId == invitation.InviterId);
            context.Invitations.Remove(inv);
        }
    }
}
