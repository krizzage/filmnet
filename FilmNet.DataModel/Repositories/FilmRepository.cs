﻿using FilmNet.DataModel.Abstract;
using FilmNet.Entities.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RefactorThis.GraphDiff;

namespace FilmNet.DataModel.Repositories
{
    public class FilmRepository : Repository<Film>, IFilmRepository
    {
        public FilmRepository(FilmNetContext context) : base(context)
        {

        }

        public void UpdateFilm(Film film)
        {
            context.UpdateGraph(film, map => map.OwnedCollection(x => x.Roles));
        }
    }
}
