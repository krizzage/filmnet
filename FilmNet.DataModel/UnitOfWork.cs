﻿using FilmNet.DataModel.Repositories;
using FilmNet.Entities.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilmNet.DataModel
{
    public class UnitOfWork : IDisposable
    {
        private FilmNetContext context = new FilmNetContext();
        private Repository<Actor> actorRepository;
        private FilmRepository filmRepository;
        private Repository<Genre> genreRepository;
        private Repository<Log> logRepository;
        private Repository<Rating> ratingRepository;
        private Repository<Role> roleRepository;
        private UserRepository userRepository;

        public Repository<Actor> ActorRepository
        {
            get
            {
                if (this.actorRepository == null)
                    this.actorRepository = new Repository<Actor>(context);

                return actorRepository;
            }
        }

        public FilmRepository FilmRepository
        {
            get
            {
                if (this.filmRepository == null)
                    this.filmRepository = new FilmRepository(context);

                return filmRepository;
            }
        }

        public Repository<Genre> GenreRepository
        {
            get
            {
                if (this.genreRepository == null)
                    this.genreRepository = new Repository<Genre>(context);
                
                return genreRepository;
            }
        }

        public Repository<Log> LogRepository
        {
            get
            {
                if (this.logRepository == null)
                    this.logRepository = new Repository<Log>(context);

                return logRepository;
            }
        }

        public Repository<Rating> RatingRepository
        {
            get
            {
                if (this.ratingRepository == null)
                    this.ratingRepository = new Repository<Rating>(context);

                return ratingRepository;
            }
        }

        public Repository<Role> RoleRepository
        {
            get
            {
                if (this.roleRepository == null)
                    this.roleRepository = new Repository<Role>(context);

                return roleRepository;
            }
        }

        public UserRepository UserRepository
        {
            get
            {
                if (this.userRepository == null)
                    this.userRepository = new UserRepository(context);

                return userRepository;
            }
        }
        
        public void Save() => context.SaveChanges();

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                    context.Dispose();
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
