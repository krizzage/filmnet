﻿using System.Data.Entity;
using FilmNet.Entities.Classes;
using System.Linq;
using System;

namespace FilmNet.DataModel
{
    public class FilmNetContext : DbContext
    {
        public FilmNetContext() :
            base("FilmNetConnectionString")
        {
            Database.SetInitializer<FilmNetContext>(new FilmNetDbInitializer());
        }

        public DbSet<Actor> Actors { get; set; }
        public DbSet<Film> Films { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<Invitation> Invitations { get; set; }
        public DbSet<Friend> Friends { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        { 
            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            var addedAuditedEntities = ChangeTracker.Entries()
                    .Where(p => p.State == EntityState.Added)
                    .Select(p => p.Entity);
            var modifiedAuditedEntities = ChangeTracker.Entries()
                  .Where(p => p.State == EntityState.Modified)
                  .Select(p => p.Entity);
            var deletedAuditedEntities = ChangeTracker.Entries()
                    .Where(p => p.State == EntityState.Deleted)
                    .Select(p => p.Entity);
            var now = DateTime.Now;


            foreach (var added in addedAuditedEntities)
            {
                if (added is Film)
                {
                    var i = (Film)added;
                    Logs.Add(new Log() { Date = DateTime.Now, Action = "Dodanie filmu : " + i.Name });
                }
                if (added is Actor)
                {
                    var i = (Actor)added;
                    Logs.Add(new Log() { Date = DateTime.Now, Action = "Dodanie aktora : " + i.Name + i.Surname});
                }
            }

            foreach (var modified in modifiedAuditedEntities)
            {
                if (modified is Film)
                {
                    var i = (Film)modified;
                    Logs.Add(new Log() { Date = DateTime.Now, Action = "Modyfikacja filmu id : " + i.FilmId });
                }
                if (modified is Actor)
                {
                    var i = (Actor)modified;
                    Logs.Add(new Log() { Date = DateTime.Now, Action = "Modyfikacja aktora id : " + i.ActorId + i.Surname});
                }
            }

            foreach (var deleted in deletedAuditedEntities)
            {
                if (deleted is Film)
                {
                    var i = (Film)deleted;
                    Logs.Add(new Log() { Date = DateTime.Now, Action = "Usuniecie filmu : " + i.Name });
                }
                if (deleted is Actor)
                {
                    var i = (Actor)deleted;
                    Logs.Add(new Log() { Date = DateTime.Now, Action = "Usuniecie aktora : " + i.Name + i.Surname});
                }
            }

            return base.SaveChanges();

        }

    }
}
