﻿using FilmNet.Entities.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilmNet.DataModel.Abstract
{
    public interface IFilmRepository : IRepository<Film>
    {
        void UpdateFilm(Film film);
    }
}
