﻿using FilmNet.Entities.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilmNet.DataModel.Abstract
{
    public interface IUserRepository : IRepository<User>
    {
        void AddInvitation(User from, User to);
        void AddFriend(int firstId, int secondId);
        IEnumerable<Friend> GetFriends(User user);
        IEnumerable<Invitation> GetPendingInvitations(User user);
        void AcceptInvitation(Invitation invitation);
        Invitation GetInvitationByUsersId(int userFrom, int userTo);
        void DeclineInvitation(Invitation invitation);
    }
}
