namespace FilmNet.DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FriendsUpdate2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Invitations", "IsAccepted", c => c.Boolean(defaultValue:null));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Invitations", "IsAccepted", c => c.Boolean(nullable: false));
        }
    }
}
