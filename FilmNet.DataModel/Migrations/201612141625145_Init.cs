namespace FilmNet.DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Actors",
                c => new
                    {
                        ActorId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Surname = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.ActorId);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        FilmId = c.Int(nullable: false),
                        ActorId = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.FilmId, t.ActorId })
                .ForeignKey("dbo.Actors", t => t.ActorId, cascadeDelete: true)
                .ForeignKey("dbo.Films", t => t.FilmId, cascadeDelete: true)
                .Index(t => t.FilmId)
                .Index(t => t.ActorId);
            
            CreateTable(
                "dbo.Films",
                c => new
                    {
                        FilmId = c.Int(nullable: false, identity: true),
                        GenreId = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                        Average = c.Double(nullable: false),
                        Description = c.String(),
                        Year = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.FilmId)
                .ForeignKey("dbo.Genres", t => t.GenreId, cascadeDelete: true)
                .Index(t => t.GenreId);
            
            CreateTable(
                "dbo.Genres",
                c => new
                    {
                        GenreId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.GenreId);
            
            CreateTable(
                "dbo.Ratings",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        FilmId = c.Int(nullable: false),
                        UserRating = c.Int(nullable: false),
                        Comment = c.String(),
                    })
                .PrimaryKey(t => new { t.UserId, t.FilmId })
                .ForeignKey("dbo.Films", t => t.FilmId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.FilmId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        Login = c.String(nullable: false, maxLength: 20, unicode: false),
                        Password = c.String(),
                        SuperUser = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserId)
                .Index(t => t.Login, unique: true);
            
            CreateTable(
                "dbo.Friends",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        InviterId = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        User_UserId = c.Int(),
                        User_UserId1 = c.Int(),
                    })
                .PrimaryKey(t => new { t.UserId, t.InviterId })
                .ForeignKey("dbo.Users", t => t.InviterId)
                .ForeignKey("dbo.Users", t => t.User_UserId)
                .ForeignKey("dbo.Users", t => t.User_UserId1)
                .Index(t => t.InviterId)
                .Index(t => t.User_UserId)
                .Index(t => t.User_UserId1);
            
            CreateTable(
                "dbo.Invitations",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        InviterId = c.Int(nullable: false),
                        IsAccepted = c.Boolean(nullable: false),
                        User_UserId = c.Int(),
                        User_UserId1 = c.Int(),
                    })
                .PrimaryKey(t => new { t.UserId, t.InviterId })
                .ForeignKey("dbo.Users", t => t.InviterId)
                .ForeignKey("dbo.Users", t => t.User_UserId)
                .ForeignKey("dbo.Users", t => t.User_UserId1)
                .Index(t => t.InviterId)
                .Index(t => t.User_UserId)
                .Index(t => t.User_UserId1);
            
            CreateTable(
                "dbo.Logs",
                c => new
                    {
                        LogId = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Action = c.String(),
                    })
                .PrimaryKey(t => t.LogId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Roles", "FilmId", "dbo.Films");
            DropForeignKey("dbo.Ratings", "UserId", "dbo.Users");
            DropForeignKey("dbo.Invitations", "User_UserId1", "dbo.Users");
            DropForeignKey("dbo.Invitations", "User_UserId", "dbo.Users");
            DropForeignKey("dbo.Invitations", "InviterId", "dbo.Users");
            DropForeignKey("dbo.Friends", "User_UserId1", "dbo.Users");
            DropForeignKey("dbo.Friends", "User_UserId", "dbo.Users");
            DropForeignKey("dbo.Friends", "InviterId", "dbo.Users");
            DropForeignKey("dbo.Ratings", "FilmId", "dbo.Films");
            DropForeignKey("dbo.Films", "GenreId", "dbo.Genres");
            DropForeignKey("dbo.Roles", "ActorId", "dbo.Actors");
            DropIndex("dbo.Invitations", new[] { "User_UserId1" });
            DropIndex("dbo.Invitations", new[] { "User_UserId" });
            DropIndex("dbo.Invitations", new[] { "InviterId" });
            DropIndex("dbo.Friends", new[] { "User_UserId1" });
            DropIndex("dbo.Friends", new[] { "User_UserId" });
            DropIndex("dbo.Friends", new[] { "InviterId" });
            DropIndex("dbo.Users", new[] { "Login" });
            DropIndex("dbo.Ratings", new[] { "FilmId" });
            DropIndex("dbo.Ratings", new[] { "UserId" });
            DropIndex("dbo.Films", new[] { "GenreId" });
            DropIndex("dbo.Roles", new[] { "ActorId" });
            DropIndex("dbo.Roles", new[] { "FilmId" });
            DropTable("dbo.Logs");
            DropTable("dbo.Invitations");
            DropTable("dbo.Friends");
            DropTable("dbo.Users");
            DropTable("dbo.Ratings");
            DropTable("dbo.Genres");
            DropTable("dbo.Films");
            DropTable("dbo.Roles");
            DropTable("dbo.Actors");
        }
    }
}
