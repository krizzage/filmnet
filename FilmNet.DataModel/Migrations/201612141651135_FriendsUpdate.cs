namespace FilmNet.DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FriendsUpdate : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Friends", name: "InviterId", newName: "UserFriendId");
            RenameIndex(table: "dbo.Friends", name: "IX_InviterId", newName: "IX_UserFriendId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Friends", name: "IX_UserFriendId", newName: "IX_InviterId");
            RenameColumn(table: "dbo.Friends", name: "UserFriendId", newName: "InviterId");
        }
    }
}
