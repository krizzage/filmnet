﻿using FilmNet.Entities.Classes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilmNet.DataModel
{
    public class FilmNetDbInitializer : DropCreateDatabaseAlways<FilmNetContext>
    {
        protected override void Seed(FilmNetContext context)
        {

        }
    }
}
