﻿using FilmNet.Entities.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace FilmNet.DataModel
{
    public static class UserInfo
    {
        public static int UserId { get; set; }
        public static string UserName { get; set; }
        public static bool IsSuperuser { get; set; }
        public static string UserPassword { get; set; }
        public static Visibility AdminControlsVisible { get; set; }

        public static void InitUser(User tmp)
        {
            UserId = tmp.UserId;
            UserName = tmp.Login;
            UserPassword = tmp.Password;
            IsSuperuser = tmp.SuperUser;
            AdminControlsVisible = IsSuperuser ? Visibility.Visible : Visibility.Hidden;
        }
    }
}
