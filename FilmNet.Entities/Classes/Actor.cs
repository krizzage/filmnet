﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilmNet.Entities.Classes
{
    public class Actor
    {
        public Actor()
        {
            Roles = new List<Role>();
        }

        public int ActorId { get; set; }
        [Required]
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Role> Roles { get; set; }
    }
}
