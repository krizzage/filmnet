﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilmNet.Entities.Classes
{
    public class Role
    {
        [Key]
        [Column(Order = 0)]
        public int FilmId { get; set; }

        [Key]
        [Column(Order = 1)]
        public int ActorId { get; set; }
        [Required]
        public string Name { get; set; }

        public virtual Film Film { get; set; }
        public virtual Actor Actor { get; set; }
    }
}
