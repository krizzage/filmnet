﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilmNet.Entities.Classes
{
    public class Film
    {
        public Film()
        {
            Roles = new List<Role>();
            Ratings = new List<Rating>();
        }

        public int FilmId { get; set; }
        public int GenreId { get; set; }
        [Required]
        public string Name { get; set; }
        public double Average { get; set; }
        public string Description { get; set; }
        [Required]
        public int Year { get; set; }
        
        public virtual Genre Genre { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
        public virtual ICollection<Rating> Ratings { get; set; }
    }
}
