﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilmNet.Entities.Classes
{
    public class User
    {
        public User()
        {
            Ratings = new List<Rating>();
            Friends = new List<Friend>();
            Invitations = new List<Invitation>();
        }

        
        public int UserId { get; set; }

        [Column(TypeName ="VARCHAR")]
        [StringLength(20)]
        [Index(IsUnique =true)]
        [Required]
        public string Login { get; set; }

        [MinLength(5)]
        public string Password { get; set; }
        public bool SuperUser { get; set; }

        public virtual ICollection<Rating> Ratings { get; set; }
        public virtual ICollection<Friend> Friends { get; set; }
        public virtual ICollection<Invitation> Invitations { get; set; }
    }
}
