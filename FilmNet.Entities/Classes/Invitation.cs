﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FilmNet.Entities.Classes
{
    public class Invitation
    {
        public Invitation()
        {

        }

        [Key]
        [Column(Order = 0)]
        public int UserId { get; set; }
        public virtual User User { get; set; }

        [Key]
        [Column(Order = 1)]
        [ForeignKey("Inviter")]
        public int InviterId { get; set; }
        public virtual User Inviter { get; set; }

        public bool ? IsAccepted { get; set; }
    }
}