﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FilmNet.Entities.Classes
{
    public class Friend
    {
        public Friend()
        {

        }

        [Key]
        [Column(Order = 0)]
        public int UserId { get; set; }
        public virtual User User { get; set; }

        [Key]
        [Column(Order = 1)]
        [ForeignKey("UserFriend")]
        public int UserFriendId { get; set; }
        public virtual User UserFriend { get; set; }

        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
    }
}