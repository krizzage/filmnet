﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilmNet.Entities.Classes
{
    public class Log
    {
        public int LogId { get; set; }
        public DateTime Date { get; set; }
        public string Action { get; set; }
    }
}
