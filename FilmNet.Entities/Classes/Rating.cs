﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilmNet.Entities.Classes
{
    public class Rating
    {
        [Key]
        [Column(Order = 0)]
        public int UserId { get; set; }

        [Key]
        [Column(Order = 1)]
        public int FilmId { get; set; }

        [Range(1, 5)]
        public int UserRating { get; set; }
        public string Comment { get; set; }

        public virtual User User { get; set; }
        public virtual Film Film { get; set; }
    }
}
