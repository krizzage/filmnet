﻿using FilmNet.DataModel.Abstract;
using FilmNet.DataModel.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FilmNet.Entities.Classes;
using System.Linq.Expressions;

namespace FilmNet.Tests.Mocks
{
    class MockFilmRepository : IFilmRepository
    {
        private List<Film> context;

        public MockFilmRepository()
        {
            Genre genreTest1 = new Genre() { GenreId = 1, Name = "TestGenre1" };
            Genre genreTest2 = new Genre() { GenreId = 2, Name = "TestGenre2" };

            context = new List<Film>
            {
                new Film() { Name = "TestFilm1", Description = "Emerytowany strażnik więzienny opowiada przyjaciółce o niezwykłym mężczyźnie, którego skazano na śmierć za zabójstwo dwóch 9-letnich dziewczynek.",
                    Genre = genreTest1, FilmId = 1, GenreId = 1
                },
                new Film() { Name = "TestFilm2", Description = "Emerytowany strażnik więzienny opowiada przyjaciółce o niezwykłym mężczyźnie, którego skazano na śmierć za zabójstwo dwóch 9-letnich dziewczynek.",
                    Genre = genreTest2, FilmId = 2, GenreId = 2
                },
                new Film() { Name = "TestFilm3", Description = "Emerytowany strażnik więzienny opowiada przyjaciółce o niezwykłym mężczyźnie, którego skazano na śmierć za zabójstwo dwóch 9-letnich dziewczynek.",
                    Genre = genreTest2, FilmId = 3, GenreId = 2
                },
                new Film() { Name = "TestFilm4", Description = "Emerytowany strażnik więzienny opowiada przyjaciółce o niezwykłym mężczyźnie, którego skazano na śmierć za zabójstwo dwóch 9-letnich dziewczynek.",
                    Genre = genreTest1, FilmId = 4, GenreId = 1
                },
                new Film() { Name = "TestFilm5", Description = "Emerytowany strażnik więzienny opowiada przyjaciółce o niezwykłym mężczyźnie, którego skazano na śmierć za zabójstwo dwóch 9-letnich dziewczynek.",
                    Genre = genreTest1, FilmId = 5, GenreId = 1
                }
            };

        }


        public void Delete(Film entityToDelete)
        {
            context.Remove(entityToDelete);
        }

        public void Delete(object id)
        {
            var toDelete = context.Find(x => x.FilmId == (int)id);
            context.Remove(toDelete);
        }

        public IEnumerable<Film> Get(Expression<Func<Film, bool>> filter = null, Func<IQueryable<Film>, IOrderedQueryable<Film>> orderBy = null, string includeProperties = "")
        {
            var films = context.ToList();
            films = films.Where(filter.Compile()).ToList();

            return films;

        }

        public Film GetByID(object id)
        {
            return context.Find(x => x.FilmId == (int)id);
        }

        public void Insert(Film entity)
        {
            context.Add(entity);
        }

        public void Save()
        {

        }

        public Film SingleOrDefault(Expression<Func<Film, bool>> predicate)
        {
            return context.SingleOrDefault(predicate.Compile());
        }

        public void Update(Film entityToUpdate)
        {
            var updateInd = context.FindIndex(x => x.FilmId == entityToUpdate.FilmId);
            context[updateInd] = entityToUpdate;
        }

        public void UpdateFilm(Film film)
        {
            this.Update(film);
        }
    }
}
