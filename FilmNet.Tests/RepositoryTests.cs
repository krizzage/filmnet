﻿namespace TestProject1
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Moq;
    using FilmNet.Entities.Classes;
    using FilmNet.DataModel.Abstract;
    using System.Linq.Expressions;    


    [TestClass]
    public class RepositoryTests
    {
        public RepositoryTests()
        {
            IList<Actor> actors = new List<Actor>
                {
                    new Actor { ActorId = 1, Name = "Actor1", Surname="Sur1", Description = "Test1"},
                    new Actor { ActorId = 2, Name = "Actor2", Surname="Sur2", Description = "Test2"},
                    new Actor { ActorId = 3, Name = "Actor3", Surname="Sur3", Description = "Test3"}
                };

            Mock<IRepository<Actor>> mockActorRepository = new Mock<IRepository<Actor>>();

            mockActorRepository.Setup(mr => mr.Get(It.IsAny<Expression<Func<Actor, bool>>>(), It.IsAny<Func<IQueryable<Actor>, IOrderedQueryable<Actor>>>(), It.IsAny<string>()))
                .Returns((Expression<Func<Actor, bool>> filter, Func<IEnumerable<Actor>, IOrderedEnumerable<Actor>> orderBy, string incl) =>
                {
                    List<Actor> query = actors.ToList();

                    if (filter != null)
                        query = query.Where(filter.Compile()).ToList();

                    if (orderBy != null)
                        return orderBy(query).ToList();
                    else
                        return query.ToList();
                });

            mockActorRepository.Setup(mr => mr.GetByID(It.IsAny<int>())).Returns((int i) => actors.Where(x => x.ActorId == i).Single());

            mockActorRepository.Setup(mr => mr.SingleOrDefault(It.IsAny<Expression<Func<Actor, bool>>>()))
                .Returns((Expression<Func<Actor, bool>> predicate) => actors.FirstOrDefault(predicate.Compile()));

            mockActorRepository.Setup(mr => mr.Update(It.IsAny<Actor>())).Callback((Actor x) =>
            {
                Actor toUpdate = actors.FirstOrDefault(s => s.ActorId == x.ActorId);
                if (toUpdate == null)
                    return;
                toUpdate.Name = x.Name;
                toUpdate.Surname = x.Surname;
                toUpdate.Description = x.Description;
            });

            mockActorRepository.Setup(mr => mr.Delete(It.IsAny<Actor>())).Callback((Actor x) =>
            {
                Actor toDelete = actors.FirstOrDefault(s => s.ActorId == x.ActorId);
                if (toDelete == null)
                    return;
                actors.Remove(toDelete);
            });

            mockActorRepository.Setup(mr => mr.Insert(It.IsAny<Actor>())).Callback((Actor x) =>
            {
                x.ActorId = actors.Count() + 1;
                actors.Add(x);
            });

            this.MockActorRepository = mockActorRepository.Object;
        }

        public TestContext TestContext { get; set; }

        public readonly IRepository<Actor> MockActorRepository;

        [TestMethod]
        public void CanReturnActorById()
        {
            Actor actor = this.MockActorRepository.GetByID(2);

            Assert.IsNotNull(actor);
            Assert.IsInstanceOfType(actor, typeof(Actor)); 
            Assert.AreEqual("Actor2", actor.Name);
            Assert.AreEqual("Sur2", actor.Surname);
        }

        [TestMethod]
        public void CanReturnAllActors()
        {
            IEnumerable<Actor> actors = this.MockActorRepository.Get(null, null,null);

            Assert.IsNotNull(actors);
            Assert.AreEqual(3, actors.Count());
        }

        [TestMethod]
        public void CanInsertActor()
        {
            Actor actor = new Actor
            { Name = "Actor4", Description = "Test4"};

            int actorsCount = this.MockActorRepository.Get(null, null,null).Count();
            Assert.AreEqual(3, actorsCount); 

            this.MockActorRepository.Insert(actor);

            actorsCount = this.MockActorRepository.Get(null, null, null).Count();
            Assert.AreEqual(4, actorsCount); 

            Actor testActor = this.MockActorRepository.SingleOrDefault(x => x.Name == "Actor4");

            Assert.IsNotNull(testActor);
            Assert.IsInstanceOfType(testActor, typeof(Actor));
            Assert.AreEqual(4, testActor.ActorId);
        }

        [TestMethod]
        public void CanUpdateActor()
        {
            Actor actor = this.MockActorRepository.GetByID(1);

            actor.Name = "NewActorName";

            this.MockActorRepository.Update(actor);

            Assert.AreEqual("NewActorName", this.MockActorRepository.GetByID(1).Name);
        }

        [TestMethod]
        public void CanDeleteActor()
        {
            Actor actor = this.MockActorRepository.GetByID(1);
            
            this.MockActorRepository.Delete(actor);

            Assert.IsNull(this.MockActorRepository.SingleOrDefault(x => x.ActorId == 1));
        }

        [TestMethod]
        public void CanFilterActors()
        {

            IEnumerable<Actor> filtered = this.MockActorRepository.Get(x => x.Name == "Actor1" || x.Name == "Actor2", null, null).ToList();

            Assert.IsNotNull(filtered);
            Assert.IsInstanceOfType(filtered, typeof(IEnumerable<Actor>));
            foreach (var e in filtered)
            {
                Assert.IsTrue(e.Surname == "Sur1" || e.Surname == "Sur2");
                Assert.IsTrue(e.Name == "Actor1" || e.Name == "Actor2");
            }
        }

    }
}