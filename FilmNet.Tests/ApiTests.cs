﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using FilmNet.Entities.Classes;
using FilmNet.DataModel.Abstract;
using System.Linq.Expressions;
using FilmNet.Api.Controllers;
using FilmNet.Tests.Mocks;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using FilmNet.DataModel.DTO;

namespace FilmNet.Tests
{

    [TestClass]
    public class ApiTests
    {
        [TestMethod]
        public void ShouldReturnFilmWithSameId()
        {
            // Arrange
            MockFilmRepository repository = new MockFilmRepository();
            var controller = new FilmsController(repository);

            // Act
            IHttpActionResult actionResult = controller.GetFilmById(2);
            var contentResult = actionResult as OkNegotiatedContentResult<FilmDTO>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(2, contentResult.Content.FilmId);
        }

        [TestMethod]
        public void ShouldNotFindFilm()
        {
            // Arrange
            MockFilmRepository repository = new MockFilmRepository();
            var controller = new FilmsController(repository);

            // Act
            IHttpActionResult actionResult = controller.GetFilmById(100);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        [TestMethod]
        public void ShouldDeleteFilm()
        {
            // Arrange
            MockFilmRepository repository = new MockFilmRepository();
            var controller = new FilmsController(repository);

            // Act
            IHttpActionResult actionResult = controller.DeleteFilm(1);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(OkResult));
        }

        [TestMethod]
        public void ShouldUpdateFilm()
        {
            // Arrange
            MockFilmRepository repository = new MockFilmRepository();
            var controller = new FilmsController(repository);
            var film = repository.GetByID(1);
            film.Name = "Testowa nazwa";

            // Act
            IHttpActionResult actionResult = controller.PutFilm(film);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(OkResult));
        }

        [TestMethod]
        public void ShouldInsertFilm()
        {
            // Arrange
            MockFilmRepository repository = new MockFilmRepository();
            var controller = new FilmsController(repository);

            // Act
            IHttpActionResult actionResult = controller.PostFilm(new Film() { FilmId = 5, Name="Test", Description = "Testowy opis" });

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(OkResult));
        }
    }
}
