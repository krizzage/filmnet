﻿using FilmNet.Entities.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FilmNet.Api.Tools
{

    public static class ExpressionBuilder
    {
        public static Expression<Func<Film, bool>> BuildFilmFilter(string _name, string _genre)
        {
            if (String.IsNullOrEmpty(_name) == false && String.IsNullOrEmpty(_genre) == false)
            {
                return x => x.Name.Contains(_name) && x.Genre.Name.Contains(_genre);
            }
            else if (String.IsNullOrEmpty(_name) == false)
            {
                return x => x.Name.Contains(_name);
            }
            else if (String.IsNullOrEmpty(_genre) == false)
            {
                return x => x.Genre.Name.Contains(_genre);
            }
            return null;
        }

        public static Func<IQueryable<Film>, IOrderedQueryable<Film>> BuildFilmOrderBy(string orderby, bool desc)
        {

            switch (orderby)
            {
                case "Tytuł":
                    return q => q.OrderBy(o => desc ? o.Name : "").ThenByDescending(o => desc ? "" : o.Name);
                case "Gatunek":
                    return q => q.OrderBy(o => desc ? o.Genre.Name : "").ThenByDescending(o => desc ? "" : o.Genre.Name);
                case "Rok":
                    return q => q.OrderBy(o => desc ? o.Year : default(int)).ThenByDescending(o => desc ? default(int) : o.Year);
                case "Średnia ocen":
                    return q => q.OrderBy(o => desc ? o.Average : default(int)).ThenByDescending(o => desc ? default(int) : o.Average);
                default:
                    return null;
            }
        }
        
        public static Expression<Func<Actor, bool>> BuildActorFilter(string search)
        {
            if(String.IsNullOrEmpty(search) == false)
                 return  x => (x.Name + " " + x.Surname).Contains(search);

            return null;
        }

        public static Func<IQueryable<Actor>, IOrderedQueryable<Actor>> BuildActorOrderBy(string orderby, bool desc)
        {
            switch (orderby)
            {
                case "Imię":
                    return q => q.OrderBy(o => desc ? o.Name : "").ThenByDescending(o => desc ? "" : o.Name);
                case "Nazwisko":
                    return q => q.OrderBy(o => desc ? o.Surname : "").ThenByDescending(o => desc ? "" : o.Surname);
                default:
                    return null;
            }
        }
    }
}
