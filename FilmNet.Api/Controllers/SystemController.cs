﻿using FilmNet.DataModel;
using FilmNet.DataModel.Abstract;
using FilmNet.DataModel.DTO;
using FilmNet.DataModel.Repositories;
using FilmNet.Entities.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FilmNet.Api.Controllers
{
    public class SystemController : ApiController
    {
        IRepository<Log> repository;

        public SystemController(IRepository<Log> _repository)
        {
            repository = _repository;
        }

        public IHttpActionResult GetLogs()
        {
            var logs = repository.Get(orderBy: x => x.OrderByDescending(y => y.Date)).Select(x => new LogDTO()
            {
                LogId = x.LogId,
                Action = x.Action,
                Date = x.Date
            });

            return Ok(logs);
        }
    }
}
