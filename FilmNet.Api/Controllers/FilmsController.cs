﻿using FilmNet.DataModel;
using FilmNet.DataModel.Repositories;
using FilmNet.Entities.Classes;
using FilmNet.DataModel.DTO;
using System;
using System.Linq;
using System.Web.Http;
using System.Linq.Expressions;
using FilmNet.Api.Tools;
using FilmNet.DataModel.Abstract;

namespace FilmNet.Api.Controllers
{
    public class FilmsController : ApiController
    {
        IFilmRepository repository;

        public FilmsController(IFilmRepository _repository)
        {
            repository = _repository;
        }

        public IHttpActionResult GetFilms(string name = "", string genre = "", string orderby = "", bool desc = false)
        {
            var filter = ExpressionBuilder.BuildFilmFilter(name, genre);
            var orderBy = ExpressionBuilder.BuildFilmOrderBy(orderby, desc);
            var films = repository.Get(filter, orderBy).Select(f => new FilmDTO()
            {
                Name = f.Name,
                FilmId = f.FilmId,
                GenreId = f.GenreId,
                Genre = new GenreDTO()
                {
                    GenreId = f.GenreId,
                    Name = f.Genre.Name
                },
                Average = f.Average,
                Description = f.Description,
                Year = f.Year,
                Roles = f.Roles.Select(r => new FilmRoleDTO()
                {
                    Actor = new ActorDTO()
                    {
                        ActorId = r.ActorId,
                        Name = r.Actor.Name,
                        Surname = r.Actor.Surname,
                        Description = r.Actor.Description
                    },
                    Name = r.Name,
                    ActorId = r.ActorId,
                    FilmId = r.FilmId

                }).ToList(),
                Ratings = f.Ratings.Select(r => new RatingDTO()
                {
                    FilmId = r.FilmId,
                    UserId = r.UserId,
                    Comment = r.Comment,
                    UserRating = r.UserRating
                }).ToList()
            });

            return Ok(films.ToList());
        }

        [HttpDelete]
        public IHttpActionResult DeleteFilm(int id)
        {
            Film film = repository.SingleOrDefault(x => x.FilmId == id);
            if (film == null)
            {
                return NotFound();
            }

            repository.Delete(id);
            repository.Save();

            return Ok();
        }

        [HttpPost]
        public IHttpActionResult PostFilm([FromBody] Film film)
        {
            if (ModelState.IsValid)
            {
                repository.Insert(film);
                repository.Save();
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPut]
        public IHttpActionResult PutFilm([FromBody] Film film)
        {
            if (ModelState.IsValid)
            {
                repository.UpdateFilm(film);
                repository.Save();
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        public IHttpActionResult GetFilmById(int id)
        {
            var f = repository.SingleOrDefault(x => x.FilmId == id);
            if (f != null)
            {
                var filmDto = new FilmDTO()
                {
                    Name = f.Name,
                    FilmId = f.FilmId,
                    GenreId = f.GenreId,
                    Genre = new GenreDTO()
                    {
                        GenreId = f.GenreId,
                        Name = f.Genre.Name
                    },
                    Average = f.Average,
                    Description = f.Description,
                    Year = f.Year,
                    Roles = f.Roles.Select(r => new FilmRoleDTO()
                    {
                        Actor = new ActorDTO()
                        {
                            ActorId = r.ActorId,
                            Name = r.Actor.Name,
                            Surname = r.Actor.Surname,
                            Description = r.Actor.Description
                        },
                        Name = r.Name,
                        ActorId = r.ActorId,
                        FilmId = r.FilmId

                    }).ToList(),
                    Ratings = f.Ratings.Select(r => new RatingDTO()
                    {
                        FilmId = r.FilmId,
                        UserId = r.UserId,
                        Comment = r.Comment,
                        UserRating = r.UserRating
                    }).ToList()
                };
                return Ok(filmDto);
            }
            else
            {
                return NotFound();
            }
        }
    }
}
