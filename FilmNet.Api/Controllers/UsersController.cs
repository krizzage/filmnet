﻿using FilmNet.DataModel;
using FilmNet.DataModel.DTO;
using FilmNet.DataModel.Repositories;
using FilmNet.Entities.Classes;
using FilmNet.DataModel.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace FilmNet.Api.Controllers
{
    public class UsersController : ApiController
    {
        IUserRepository repository;

        public UsersController(IUserRepository _repository)
        {
            repository = _repository;
        }

        public IHttpActionResult Login([FromBody] UserDTO userDto)
        {
            var user = repository.SingleOrDefault(x => x.Login == userDto.Login && x.Password == userDto.Password);
            if (user == null)
            {
                return NotFound();
            }
            return Ok(new UserDTO() { UserId = user.UserId, Login = user.Login, Password = user.Password, SuperUser = user.SuperUser });
        }

        public IHttpActionResult GetUserById(int id)
        {
            var user = repository.SingleOrDefault(x => x.UserId == id);
            if (user == null)
            {
                return NotFound();
            }
            return Ok(new UserDTO() { Login = user.Login, UserId = user.UserId });
        }

        public IHttpActionResult GetUserByLogin(string login)
        {
            var user = repository.SingleOrDefault(x => x.Login == login);
            if (user == null)
            {
                return NotFound();
            }
            return Ok(new UserDTO() { Login = user.Login, UserId = user.UserId });
        }

        public IHttpActionResult GetUsersRatings(int id)
        {
            var user = repository.SingleOrDefault(x => x.UserId == id);
            if (user == null)
            {
                return NotFound();
            }
            else
            {
                var ratings = user.Ratings.Select(x => new RatingDTO()
                {
                    UserRating = x.UserRating,
                    Film = new FilmDTO() { Name = x.Film.Name }
                });

                return Ok(ratings);
            }
        }

        public IHttpActionResult GetUserFriends(int id)
        {
            var user = repository.SingleOrDefault(x => x.UserId == id);
            if (user == null)
            {
                return NotFound();
            }
            else
            {
                var friends = repository.GetFriends(user).Select(u => new FriendDTO()
                {
                    UserId = u.UserId,
                    UserFriendId = u.UserFriendId,
                    UserFriend = new UserDTO()
                    {
                        UserId = u.UserFriend.UserId,
                        Login = u.UserFriend.Login
                    },
                    Date = u.Date
                }).ToList();
                return Ok(friends);
            }
        }

        public IHttpActionResult GetUserInvitations(int id)
        {
            var user = repository.SingleOrDefault(x => x.UserId == id);
            if (user == null)
            {
                return NotFound();
            }
            else
            {
                var invitations = repository.GetPendingInvitations(user).Select(u => new InvitationDTO()
                {
                    UserId = u.UserId,
                    InviterId = u.InviterId,
                    Inviter = new UserDTO()
                    {
                        UserId = u.Inviter.UserId,
                        Login = u.Inviter.Login
                    },
                    IsAccepted = u.IsAccepted
                }).ToList();
                return Ok(invitations);
            }
        }

        public IHttpActionResult PostInvitation([FromBody] InvitationDTO newInvitation)
        {
            var userFrom = repository.GetByID(newInvitation.UserId);
            var userTo = repository.GetByID(newInvitation.InviterId);
            if (userFrom == null || userTo == null)
            {
                return NotFound();
            }
            else
            {
                repository.AddInvitation(userFrom, userTo);
                repository.Save();
                return Ok();
            }
        }

        public IHttpActionResult AcceptInvitation([FromBody] InvitationDTO newInvitation)
        {
            var userFrom = repository.GetByID(newInvitation.UserId);
            var userTo = repository.GetByID(newInvitation.InviterId);
            if (userFrom == null || userTo == null)
            {
                return NotFound();
            }
            else
            {
                var inv = repository.GetInvitationByUsersId(userFrom.UserId, userTo.UserId);
                repository.AcceptInvitation(inv);
                repository.Save();
                return Ok();
            }
        }

        public IHttpActionResult DeclineInvitation([FromBody] InvitationDTO newInvitation)
        {
            var userFrom = repository.GetByID(newInvitation.UserId);
            var userTo = repository.GetByID(newInvitation.InviterId);
            if (userFrom == null || userTo == null)
            {
                return NotFound();
            }
            else
            {
                var inv = repository.GetInvitationByUsersId(userFrom.UserId, userTo.UserId);
                repository.DeclineInvitation(inv);
                repository.Save();
                return Ok();
            }
        }

        [HttpPut]
        public IHttpActionResult PutUser([FromBody] User user)
        {
            if (ModelState.IsValid)
            {
                repository.Update(user);
                repository.Save();
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPost]
        public IHttpActionResult PostUser([FromBody] User user)
        {
            if (ModelState.IsValid)
            {
                repository.Insert(user);
                repository.Save();
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpDelete]
        public IHttpActionResult DeleteUser(int id)
        {
            User user = repository.SingleOrDefault(x => x.UserId == id);
            if (user == null)
            {
                return NotFound();
            }

            repository.Delete(id);
            repository.Save();

            return Ok();
        }


    }
}
