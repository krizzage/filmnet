﻿using FilmNet.Api.Tools;
using FilmNet.DataModel;
using FilmNet.DataModel.Abstract;
using FilmNet.DataModel.DTO;
using FilmNet.DataModel.Repositories;
using FilmNet.Entities.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FilmNet.Api.Controllers
{
    public class ActorsController : ApiController
    {
        IRepository<Actor> repository;

        public ActorsController(IRepository<Actor> _repository)
        {
            repository = _repository;
        }

        public IHttpActionResult GetActors(string name = "", string orderby = "", bool desc = false)
        {
            var filter = ExpressionBuilder.BuildActorFilter(name);
            var orderBy = ExpressionBuilder.BuildActorOrderBy(orderby, desc);
            var actors = repository.Get(filter, orderBy).Select(a => new ActorDTO()
            {
                ActorId = a.ActorId,
                Name = a.Name,
                Surname = a.Surname,
                Description = a.Description,
                Roles = a.Roles.Select(r => new ActorRoleDTO()
                {
                    ActorId = r.ActorId,
                    FilmId = r.FilmId,
                    Name = r.Name,
                    Film = new FilmDTO()
                    {
                        FilmId = r.FilmId,
                        Name = r.Film.Name
                    }
                }).ToList()
            });

            return Ok(actors.ToList());
        }

        [HttpDelete]
        public IHttpActionResult DeleteActor(int id)
        {
            var actor = repository.SingleOrDefault(x => x.ActorId == id);
            if (actor == null)
            {
                return NotFound();
            }
            repository.Delete(id);
            repository.Save();

            return Ok();
        }

        [HttpPost]
        public IHttpActionResult PostActor([FromBody] Actor actor)
        {
            if (ModelState.IsValid)
            {
                repository.Insert(actor);
                repository.Save();
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPut]
        public IHttpActionResult PutActor([FromBody] Actor actor)
        {
            if (ModelState.IsValid)
            {
                repository.Update(actor);
                repository.Save();
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
