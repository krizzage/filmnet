﻿using FilmNet.Api.Tools;
using FilmNet.DataModel;
using FilmNet.DataModel.Abstract;
using FilmNet.DataModel.DTO;
using FilmNet.DataModel.Repositories;
using FilmNet.Entities.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FilmNet.Api.Controllers
{
    public class GenresController : ApiController
    {
        IRepository<Genre> repository;

        public GenresController(IRepository<Genre> _repository)
        {
            repository = _repository;
        }

        public IHttpActionResult GetGenres()
        {
            var genres = repository.Get().Select(g => new GenreDTO()
            {
                GenreId = g.GenreId,
                Name = g.Name
            });

            return Ok(genres.ToList());

        }

        public IHttpActionResult GetGenreByName(string name)
        {
            var genre = repository.SingleOrDefault(x => x.Name == name);
            return Ok(new GenreDTO() { GenreId = genre.GenreId, Name = genre.Name });
        }

        public IHttpActionResult GetGenreById(int id)
        {
            var genre = repository.SingleOrDefault(x => x.GenreId == id);
            return Ok(new GenreDTO() { GenreId = genre.GenreId, Name = genre.Name });
        }
    }
}
