﻿using FilmNet.Api.Tools;
using FilmNet.DataModel;
using FilmNet.DataModel.Abstract;
using FilmNet.DataModel.DTO;
using FilmNet.DataModel.Repositories;
using FilmNet.Entities.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FilmNet.Api.Controllers
{
    public class RolesController : ApiController
    {
        IRepository<Role> repository;

        public RolesController(IRepository<Role> _repository)
        {
            repository = _repository;
        }

        public IHttpActionResult GetActorRoles(int actorid)
        {
            var roles = repository.Get(x => x.ActorId == actorid).Select(r => new ActorRoleDTO()
            {
                ActorId = r.ActorId,
                FilmId = r.FilmId,
                Film = new FilmDTO()
                {
                    FilmId = r.FilmId,
                    Name = r.Film.Name
                },
                Name = r.Name
            });

            return Ok(roles);
        }

        public IHttpActionResult GetFilmRoles(int filmid)
        {
            var roles = repository.Get(x => x.FilmId == filmid, includeProperties: "Actor").Select(r => new FilmRoleDTO()
            {
                Name = r.Name,
                FilmId = r.FilmId,
                ActorId = r.ActorId,
                Actor = new ActorDTO()
                {
                    Name = r.Actor.Name,
                    Surname = r.Actor.Surname,
                    ActorId = r.ActorId
                }
            });
            return Ok(roles);
        }
    }
}
