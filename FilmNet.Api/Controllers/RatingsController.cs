﻿using FilmNet.Api.Tools;
using FilmNet.DataModel;
using FilmNet.DataModel.Abstract;
using FilmNet.DataModel.DTO;
using FilmNet.DataModel.Repositories;
using FilmNet.Entities.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FilmNet.Api.Controllers
{
    public class RatingsController : ApiController
    {
        IRepository<Rating> repository;

        public RatingsController(IRepository<Rating> _repository)
        {
            repository = _repository;
        }

        public IHttpActionResult GetRatingsByFilmId(int filmid)
        {
            var ratings = repository.Get(x => x.FilmId == filmid).Select(r => new RatingDTO()
            {
                UserId = r.UserId,
                FilmId = r.FilmId,
                Comment = r.Comment,
                UserRating = r.UserRating,
                User = new UserDTO()
                {
                    UserId = r.User.UserId,
                    Login = r.User.Login
                }
            });

            return Ok(ratings.ToList());
        }

        public IHttpActionResult GetRating(int userid, int filmid)
        {
            var r = repository.SingleOrDefault(x => x.UserId == userid && x.FilmId == filmid);

            if (r != null)
            {
                var ratingDto = new RatingDTO()
                {
                    UserId = r.UserId,
                    FilmId = r.FilmId,
                    Comment = r.Comment,
                    UserRating = r.UserRating,
                    User = new UserDTO()
                    {
                        UserId = r.User.UserId,
                        Login = r.User.Login
                    }
                };
                return Ok(ratingDto);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        public IHttpActionResult PostRating([FromBody] Rating rating)
        {
            if (ModelState.IsValid)
            {
                repository.Insert(rating);
                repository.Save();
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPut]
        public IHttpActionResult PutRating([FromBody] Rating rating)
        {
            if (ModelState.IsValid)
            {
                repository.Update(rating);
                repository.Save();
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
