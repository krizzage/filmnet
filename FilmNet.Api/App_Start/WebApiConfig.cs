﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Microsoft.Practices.Unity;
using FilmNet.Api.Resolver;
using FilmNet.DataModel.Abstract;
using FilmNet.Entities.Classes;
using FilmNet.DataModel.Repositories;

namespace FilmNet.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            var container = new UnityContainer();
            container.RegisterType<IUserRepository, UserRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IFilmRepository, FilmRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IRepository<Actor>, Repository<Actor>>(new HierarchicalLifetimeManager());
            container.RegisterType<IRepository<Film>, Repository<Film>>(new HierarchicalLifetimeManager());
            container.RegisterType<IRepository<Friend>, Repository<Friend>>(new HierarchicalLifetimeManager());
            container.RegisterType<IRepository<Genre>, Repository<Genre>>(new HierarchicalLifetimeManager());
            container.RegisterType<IRepository<Invitation>, Repository<Invitation>>(new HierarchicalLifetimeManager());
            container.RegisterType<IRepository<Log>, Repository<Log>>(new HierarchicalLifetimeManager());
            container.RegisterType<IRepository<Rating>, Repository<Rating>>(new HierarchicalLifetimeManager());
            container.RegisterType<IRepository<Role>, Repository<Role>>(new HierarchicalLifetimeManager());
            container.RegisterType<IRepository<User>, Repository<User>>(new HierarchicalLifetimeManager());

            config.DependencyResolver = new UnityResolver(container);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
