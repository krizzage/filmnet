﻿using FilmNet.DataModel;
using FirstFloor.ModernUI.Windows.Controls;

namespace FilmNet
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();

            if (!UserInfo.IsSuperuser)
                AdminPanel.DisplayName = "";
        }

    }
}