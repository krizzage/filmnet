﻿using AutoMapper;
using FilmNet.DataModel.DTO;
using FilmNet.Entities.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilmNet.Tools
{
    public static class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<ActorDTO, Actor>();
                cfg.CreateMap<GenreDTO, Genre>();
                cfg.CreateMap<FilmDTO, Film>()
                .ForMember(s => s.Roles, c => c.MapFrom(m => m.Roles))
                .ForMember(s => s.Genre, c => c.MapFrom(m => m.Genre));
                cfg.CreateMap<FilmRoleDTO, Role>()
                .ForMember(s => s.Actor, c => c.MapFrom(m => m.Actor));
                cfg.CreateMap<UserDTO, User>();
                cfg.CreateMap<ActorRoleDTO, Role>()
                .ForMember(s => s.Film, c => c.MapFrom(m => m.Film));
                cfg.CreateMap<RatingDTO, Rating>()
                .ForMember(s => s.User, c => c.MapFrom(m => m.User));
                cfg.CreateMap<LogDTO, Log>();
                cfg.CreateMap<FriendDTO, Friend>();
                cfg.CreateMap<InvitationDTO, Invitation>();
            });

        }
    }
}
