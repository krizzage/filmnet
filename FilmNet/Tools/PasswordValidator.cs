﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace FilmNet
{
    public class PasswordValidator
    {
        public bool passwordValidation { get; set; }
        private PasswordBox passwordTextBox;
        private PasswordBox confirmPasswordTextBox;
        private TextBlock validationTextBox;

        public PasswordValidator(PasswordBox passwordTextBox, PasswordBox confirmPasswordTextBox, TextBlock validationTextBox)
        {
            this.passwordTextBox = passwordTextBox;
            this.confirmPasswordTextBox = confirmPasswordTextBox;
            this.validationTextBox = validationTextBox;
        }

        public bool Validate()
        {
            return validatePasswordLenth() && validatePasswordEquality();
        }

        private bool validatePasswordLenth()
        {
            if (passwordTextBox.Password.Length < 5)
            {
                validationTextBox.Text = "Hasło musi posiadać przynajmniej 5 znaków";
                return false;
            }
            else
            {
                validationTextBox.Text = "";
                return true;
            }
        }

        private bool validatePasswordEquality()
        {
            if (!passwordTextBox.Password.Equals(confirmPasswordTextBox.Password))
            {
                validationTextBox.Text = "Hasła muszą być takie same";
                return false;
            }
            else
            {
                validationTextBox.Text = "";
                return true;
            }
        }
    }
}
