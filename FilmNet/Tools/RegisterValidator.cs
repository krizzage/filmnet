﻿using FilmNet.DataModel;
using FilmNet.Entities.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using FilmNet.Pages;
using FilmNet.Tools.Client;

namespace FilmNet
{
    public class RegisterValidator
    {
        private bool passwordValidation, loginValidation;
        private Register register;
        private PasswordValidator passwordValidator;

        public RegisterValidator(Register register)
        {
            this.register = register;
            this.passwordValidator = new PasswordValidator(register.PasswordTextBox, register.ConfirmPasswordTextBox, register.ValidationTextBox);
        }


        public async Task ValidateLogin()
        {
            if (register.LoginTextBox.Text == "")
            {
                loginValidation = false;
            }
            else
            {
                User tmp = await UserClient.GetUserByLogin(register.LoginTextBox.Text);
                if (tmp != null)
                {
                    register.ValidationTextBox.Text = "Użytkownik o takim loginie istnieje";
                    loginValidation = false;
                }
                else
                {
                    loginValidation = true;
                    register.ValidationTextBox.Text = "";
                }
            }
            SetRegisterEnable();
        }

        public void  ValidatePassword()
        {
            passwordValidation = passwordValidator.Validate();
            SetRegisterEnable();
        }
   
        private void SetRegisterEnable()
        {
            register.RegisterButton.IsEnabled = loginValidation && passwordValidation;
        }
    }
}
