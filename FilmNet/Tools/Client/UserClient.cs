﻿using FilmNet.DataModel.DTO;
using FilmNet.Entities.Classes;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FilmNet.Tools.Client
{
    public class UserClient
    {
        public static HttpClient Initialize()
        {
            HttpClient client = Client.Initialize();
            client.BaseAddress = new Uri("https://microsoft-apiappd9f45e6e079044e1b6999233716ec27d.azurewebsites.net/api/users/");
            return client;
        }

        public async static Task<User> Login(string login, string password)
        {
            using (HttpClient client = Initialize())
            {
                var content = new UserDTO() { Login = login, Password = password };
                HttpResponseMessage response = await client.PostAsJsonAsync("login", content);

                if (response.IsSuccessStatusCode)
                {
                    UserDTO dto = await response.Content.ReadAsAsync<UserDTO>();
                    return AutoMapper.Mapper.Map<User>(dto);
                }
                else
                {
                    return null;
                }
            }
        }

        public async static Task<User> GetUserById(int id)
        {
            using (HttpClient client = Initialize())
            {
                HttpResponseMessage response = await client.GetAsync($"getuserbyid?id={id}");

                if (response.IsSuccessStatusCode)
                {
                    return AutoMapper.Mapper.Map<User>(await response.Content.ReadAsAsync<UserDTO>());
                }
                else
                {
                    ModernDialog.ShowMessage("Bład api. Status code: " + response.StatusCode + "\nReason phrase: " + response.ReasonPhrase, "Błąd", System.Windows.MessageBoxButton.OK);
                    return null;
                }
            }
        }

        public async static Task<User> GetUserByLogin(string login)
        {
            using (HttpClient client = Initialize())
            {
                HttpResponseMessage response = await client.GetAsync($"getuserbylogin?login={login}");

                if (response.IsSuccessStatusCode)
                {
                    return AutoMapper.Mapper.Map<User>(await response.Content.ReadAsAsync<UserDTO>());
                }
                else
                {
                    return null;
                }
            }
        }

        public async static Task<IEnumerable<Friend>> GetUserFriends(int id)
        {
            using (HttpClient client = Initialize())
            {
                HttpResponseMessage response = await client.GetAsync($"getuserfriends?id={id}");

                if (response.IsSuccessStatusCode)
                {
                    return AutoMapper.Mapper.Map<IEnumerable<Friend>>(await response.Content.ReadAsAsync<IEnumerable<FriendDTO>>());
                }
                else
                {
                    ModernDialog.ShowMessage("Bład api. Status code: " + response.StatusCode + "\nReason phrase: " + response.ReasonPhrase, "Błąd", System.Windows.MessageBoxButton.OK);
                    return null;
                }
            }
        }

        public async static Task<IEnumerable<Rating>> GetUserRatings(int id)
        {
            using (HttpClient client = Initialize())
            {
                HttpResponseMessage response = await client.GetAsync($"getusersratings?id={id}");

                if (response.IsSuccessStatusCode)
                {
                    return AutoMapper.Mapper.Map<IEnumerable<Rating>>(await response.Content.ReadAsAsync<IEnumerable<RatingDTO>>());
                }
                else
                {
                    ModernDialog.ShowMessage("Bład api. Status code: " + response.StatusCode + "\nReason phrase: " + response.ReasonPhrase, "Błąd", System.Windows.MessageBoxButton.OK);
                    return null;
                }
            }
        }

        public async static Task<IEnumerable<Invitation>> GetUserInvitations(int id)
        {
            using (HttpClient client = Initialize())
            {
                HttpResponseMessage response = await client.GetAsync($"getuserinvitations?id={id}");

                if (response.IsSuccessStatusCode)
                {
                    return AutoMapper.Mapper.Map<IEnumerable<Invitation>>(await response.Content.ReadAsAsync<IEnumerable<InvitationDTO>>());
                }
                else
                {
                    ModernDialog.ShowMessage("Bład api. Status code: " + response.StatusCode + "\nReason phrase: " + response.ReasonPhrase, "Błąd", System.Windows.MessageBoxButton.OK);
                    return null;
                }
            }
        }

        public async static Task AddInvitation(InvitationDTO invitation)
        {
            using (HttpClient client = Initialize())
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("postinvitation", invitation);

                if (response.IsSuccessStatusCode == false)
                {
                    ModernDialog.ShowMessage("Bład api. Status code: " + response.StatusCode + "\nReason phrase: " + response.ReasonPhrase, "Błąd", System.Windows.MessageBoxButton.OK);
                }
            }
        }

        public async static Task AcceptInvitation(InvitationDTO invitation)
        {
            using (HttpClient client = Initialize())
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("acceptinvitation", invitation);

                if (response.IsSuccessStatusCode == false)
                {
                    ModernDialog.ShowMessage("Bład api. Status code: " + response.StatusCode + "\nReason phrase: " + response.ReasonPhrase, "Błąd", System.Windows.MessageBoxButton.OK);
                }
            }
        }

        public async static Task DeclineInvitation(InvitationDTO invitation)
        {
            using (HttpClient client = Initialize())
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("declineinvitation", invitation);

                if (response.IsSuccessStatusCode == false)
                {
                    ModernDialog.ShowMessage("Bład api. Status code: " + response.StatusCode + "\nReason phrase: " + response.ReasonPhrase, "Błąd", System.Windows.MessageBoxButton.OK);
                }
            }
        }

        public async static Task UpdateUser(User user)
        {
            using (HttpClient client = Initialize())
            {
                HttpResponseMessage response = await client.PutAsJsonAsync("putuser", user);

                if (response.IsSuccessStatusCode == false)
                {
                    ModernDialog.ShowMessage("Bład api. Status code: " + response.StatusCode + "\nReason phrase: " + response.ReasonPhrase, "Błąd", System.Windows.MessageBoxButton.OK);
                }
            }
        }

        public async static Task<bool> AddUser(User user)
        {
            using (HttpClient client = Initialize())
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("postuser", user);

                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    ModernDialog.ShowMessage("Dane niepoprawnie wypełnione", "Nieprawidłowe dane", System.Windows.MessageBoxButton.OK);
                    return false;
                }
                else
                {
                    ModernDialog.ShowMessage("Bład api. Status code: " + response.StatusCode + "\nReason phrase: " + response.ReasonPhrase, "Błąd", System.Windows.MessageBoxButton.OK);
                    return false;
                }
            }
        }

        public async static Task DeleteUser(int id)
        {
            using (HttpClient client = Initialize())
            {
                HttpResponseMessage response = await client.DeleteAsync($"deleteuser?id={id}");
                if (response.IsSuccessStatusCode == false)
                {
                    ModernDialog.ShowMessage("Bład api. Status code: " + response.StatusCode + "\nReason phrase: " + response.ReasonPhrase, "Błąd", System.Windows.MessageBoxButton.OK);
                }
            }
        }
    }
}
