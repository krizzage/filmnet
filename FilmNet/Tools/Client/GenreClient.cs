﻿using FilmNet.DataModel.DTO;
using FilmNet.Entities.Classes;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FilmNet.Tools.Client
{
    public class GenreClient
    {
        public static HttpClient Initialize()
        {
            HttpClient client = Client.Initialize();
            client.BaseAddress = new Uri("https://microsoft-apiappd9f45e6e079044e1b6999233716ec27d.azurewebsites.net/api/genres/");
            return client;
        }

        public async static Task<IEnumerable<Genre>> GetGenres()
        {
            HttpClient client = Initialize();
            HttpResponseMessage response = await client.GetAsync("getgenres");
            if (response.IsSuccessStatusCode)
            {
                return AutoMapper.Mapper.Map<IEnumerable<Genre>>(await response.Content.ReadAsAsync<IEnumerable<GenreDTO>>());
            }
            else
            {
                ModernDialog.ShowMessage("Bład api. Status code: " + response.StatusCode + "\nReason phrase: " + response.ReasonPhrase, "Błąd", System.Windows.MessageBoxButton.OK);
                return null;
            }
        }

        public async static Task<Genre> GetGenreByName(string name)
        {
            HttpClient client = Initialize();
            HttpResponseMessage response = await client.GetAsync($"getgenrebyname?name={name}");
            if (response.IsSuccessStatusCode)
            {
                return AutoMapper.Mapper.Map<Genre>(await response.Content.ReadAsAsync<GenreDTO>());
            }
            else
            {
                ModernDialog.ShowMessage("Bład api. Status code: " + response.StatusCode + "\nReason phrase: " + response.ReasonPhrase, "Błąd", System.Windows.MessageBoxButton.OK);
                return null;
            }
        }

        public async static Task<Genre> GetGenreById(int id)
        {
            HttpClient client = Initialize();
            HttpResponseMessage response = await client.GetAsync($"getgenrebyid?id={id}");
            if (response.IsSuccessStatusCode)
            {
                return AutoMapper.Mapper.Map<Genre>(await response.Content.ReadAsAsync<GenreDTO>());
            }
            else
            {
                ModernDialog.ShowMessage("Bład api. Status code: " + response.StatusCode + "\nReason phrase: " + response.ReasonPhrase, "Błąd", System.Windows.MessageBoxButton.OK);
                return null;
            }
        }
    }
}
