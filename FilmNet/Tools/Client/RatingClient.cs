﻿using FilmNet.DataModel.DTO;
using FilmNet.Entities.Classes;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FilmNet.Tools.Client
{
    public class RatingClient
    {
        public static HttpClient Initialize()
        {
            HttpClient client = Client.Initialize();
            client.BaseAddress = new Uri("https://microsoft-apiappd9f45e6e079044e1b6999233716ec27d.azurewebsites.net/api/ratings/");
            return client;
        }

        public async static Task<IEnumerable<Rating>> GetRatingsByFilmId(int filmid)
        {
            HttpClient client = Initialize();
            HttpResponseMessage response = await client.GetAsync($"getratingsbyfilmid?filmid={filmid}");
            if (response.IsSuccessStatusCode)
            {
                return AutoMapper.Mapper.Map<IEnumerable<Rating>>(await response.Content.ReadAsAsync<IEnumerable<RatingDTO>>());
            }
            else
            {
                ModernDialog.ShowMessage("Bład api. Status code: " + response.StatusCode + "\nReason phrase: " + response.ReasonPhrase, "Błąd", System.Windows.MessageBoxButton.OK);
                return null;
            }
        }

        public async static Task<Rating> GetRating(int userid, int filmid)
        {
            HttpClient client = Initialize();
            HttpResponseMessage response = await client.GetAsync($"getrating?userid={userid}&filmid={filmid}");
            if (response.IsSuccessStatusCode)
            {
                return AutoMapper.Mapper.Map<Rating>(await response.Content.ReadAsAsync<RatingDTO>());
            }
            else
                return null;
        }


        public async static Task<bool> AddRating(Rating rating)
        {
            using (HttpClient client = Initialize())
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("postrating", rating);

                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    ModernDialog.ShowMessage("Dane niepoprawnie wypełnione", "Nieprawidłowe dane", System.Windows.MessageBoxButton.OK);
                    return false;
                }
                else
                {
                    ModernDialog.ShowMessage("Bład api. Status code: " + response.StatusCode + "\nReason phrase: " + response.ReasonPhrase, "Błąd", System.Windows.MessageBoxButton.OK);
                    return false;
                }
            }
        }

        public async static Task<bool> UpdateRating(Rating rating)
        {
            using (HttpClient client = Initialize())
            {
                HttpResponseMessage response = await client.PutAsJsonAsync("putrating", rating);

                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    ModernDialog.ShowMessage("Dane niepoprawnie wypełnione", "Nieprawidłowe dane", System.Windows.MessageBoxButton.OK);
                    return false;
                }
                else
                {
                    ModernDialog.ShowMessage("Bład api. Status code: " + response.StatusCode + "\nReason phrase: " + response.ReasonPhrase, "Błąd", System.Windows.MessageBoxButton.OK);
                    return false;
                }
            }
        }
    }
}
