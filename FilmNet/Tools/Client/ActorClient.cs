﻿using FilmNet.DataModel.DTO;
using FilmNet.Entities.Classes;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FilmNet.Tools.Client
{
    public static class ActorClient
    {
        public static HttpClient Initialize()
        {
            HttpClient client = Client.Initialize();
            client.BaseAddress = new Uri("https://microsoft-apiappd9f45e6e079044e1b6999233716ec27d.azurewebsites.net/api/actors/");
            return client;
        }

        public async static Task<IEnumerable<Actor>> GetActors(string name = "", string orderby = "", bool desc = false)
        {
            HttpClient client = Initialize();
            HttpResponseMessage response = await client.GetAsync($"getactors?name={name}&orderby={orderby}&desc={desc}");
            if (response.IsSuccessStatusCode)
            {
                return AutoMapper.Mapper.Map<IEnumerable<Actor>>(await response.Content.ReadAsAsync<IEnumerable<ActorDTO>>());
            }
            else
            {
                ModernDialog.ShowMessage("Bład api. Status code: " + response.StatusCode + "\nReason phrase: " + response.ReasonPhrase, "Błąd", System.Windows.MessageBoxButton.OK);
                return null;
            }
        }

        public async static Task DeleteActor(int id)
        {
            using (HttpClient client = Initialize())
            {
                HttpResponseMessage response = await client.DeleteAsync($"deleteactor?id={id}");
                if (response.IsSuccessStatusCode)
                {
                    ModernDialog.ShowMessage("Aktor został usunięty", "Aktor usunięty", System.Windows.MessageBoxButton.OK);
                }
                else
                {
                    ModernDialog.ShowMessage("Bład api. Status code: " + response.StatusCode + "\nReason phrase: " + response.ReasonPhrase, "Błąd", System.Windows.MessageBoxButton.OK);
                }
            }
        }

        public async static Task<bool> AddActor(Actor actor)
        {
            using (HttpClient client = Initialize())
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("postactor", actor);

                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    ModernDialog.ShowMessage("Dane niepoprawnie wypełnione", "Nieprawidłowe dane", System.Windows.MessageBoxButton.OK);
                    return false;
                }
                else
                {
                    ModernDialog.ShowMessage("Bład api. Status code: " + response.StatusCode + "\nReason phrase: " + response.ReasonPhrase, "Błąd", System.Windows.MessageBoxButton.OK);
                    return false;
                }
            }
        }

        public async static Task<bool> UpdateActor(Actor actor)
        {
            using (HttpClient client = Initialize())
            {
                HttpResponseMessage response = await client.PutAsJsonAsync("putactor", actor);

                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    ModernDialog.ShowMessage("Dane niepoprawnie wypełnione", "Nieprawidłowe dane", System.Windows.MessageBoxButton.OK);
                    return false;
                }
                else
                {
                    ModernDialog.ShowMessage("Bład api. Status code: " + response.StatusCode + "\nReason phrase: " + response.ReasonPhrase, "Błąd", System.Windows.MessageBoxButton.OK);
                    return false;
                }
            }
        }
    }
}
