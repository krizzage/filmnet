﻿using FilmNet.DataModel.DTO;
using FilmNet.Entities.Classes;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FilmNet.Tools.Client
{
    public class SystemClient
    {
        public static HttpClient Initialize()
        {
            HttpClient client = Client.Initialize();
            client.BaseAddress = new Uri("https://microsoft-apiappd9f45e6e079044e1b6999233716ec27d.azurewebsites.net/api/system/");
            return client;
        }

        public async static Task<IEnumerable<Log>> GetLogs()
        {
            HttpClient client = Initialize();
            HttpResponseMessage response = await client.GetAsync($"getlogs");
            if (response.IsSuccessStatusCode)
            {
                return AutoMapper.Mapper.Map<IEnumerable<Log>>(await response.Content.ReadAsAsync<IEnumerable<LogDTO>>());
            }
            else
            {
                ModernDialog.ShowMessage("Bład api. Status code: " + response.StatusCode + "\nReason phrase: " + response.ReasonPhrase, "Błąd", System.Windows.MessageBoxButton.OK);
                return null;
            }
        }
    }
}
