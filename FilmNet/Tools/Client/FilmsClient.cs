﻿using FilmNet.DataModel.DTO;
using FilmNet.Entities.Classes;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FilmNet.Tools.Client
{
    public class FilmsClient
    {
        public static HttpClient Initialize()
        {
            HttpClient client = Client.Initialize();
            client.BaseAddress = new Uri("https://microsoft-apiappd9f45e6e079044e1b6999233716ec27d.azurewebsites.net/api/films/");
            return client;
        }

        public async static Task<IEnumerable<Film>> GetFilms(string name = "", string genre = "", string orderby = "", bool desc = false)
        {
            HttpClient client = Initialize();
            HttpResponseMessage response = await client.GetAsync($"getfilms?name={name}&genre={genre}&orderby={orderby}&desc={desc}");
            if (response.IsSuccessStatusCode)
            {
                return AutoMapper.Mapper.Map<IEnumerable<Film>>(await response.Content.ReadAsAsync<IEnumerable<FilmDTO>>());
            }
            else
            {
                ModernDialog.ShowMessage("Bład api. Status code: " + response.StatusCode + "\nReason phrase: " + response.ReasonPhrase, "Błąd", System.Windows.MessageBoxButton.OK);
                return null;
            }
        }

        public async static Task<Film> GetFilmById(int id)
        {
            HttpClient client = Initialize();
            HttpResponseMessage response = await client.GetAsync($"getfilmbyid?id={id}");
            if (response.IsSuccessStatusCode)
            {
                return AutoMapper.Mapper.Map<Film>(await response.Content.ReadAsAsync<FilmDTO>());
            }
            else
            {
                ModernDialog.ShowMessage("Bład api. Status code: " + response.StatusCode + "\nReason phrase: " + response.ReasonPhrase, "Błąd", System.Windows.MessageBoxButton.OK);
                return null;
            }
        }

        public async static Task DeleteFilm(int id)
        {
            using (HttpClient client = Initialize())
            {
                HttpResponseMessage response = await client.DeleteAsync($"deletefilm?id={id}");
                if (response.IsSuccessStatusCode)
                {
                    ModernDialog.ShowMessage("Film został usunięty", "Film usunięty", System.Windows.MessageBoxButton.OK);
                }
                else
                {
                    ModernDialog.ShowMessage("Bład api. Status code: " + response.StatusCode + "\nReason phrase: " + response.ReasonPhrase, "Błąd", System.Windows.MessageBoxButton.OK);
                }
            }
        }

        public async static Task<bool> AddFilm(Film film)
        {
            using (HttpClient client = Initialize())
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("postfilm", film);

                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    ModernDialog.ShowMessage("Dane niepoprawnie wypełnione", "Nieprawidłowe dane", System.Windows.MessageBoxButton.OK);
                    return false;
                }
                else
                {
                    ModernDialog.ShowMessage("Bład api. Status code: " + response.StatusCode + "\nReason phrase: " + response.ReasonPhrase, "Błąd", System.Windows.MessageBoxButton.OK);
                    return false;
                }
            }
        }

        public async static Task<bool> UpdateFilm(Film film)
        {
            using (HttpClient client = Initialize())
            {
                HttpResponseMessage response = await client.PutAsJsonAsync("putfilm", film);

                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    ModernDialog.ShowMessage("Dane niepoprawnie wypełnione", "Nieprawidłowe dane", System.Windows.MessageBoxButton.OK);
                    return false;
                }
                else
                {
                    ModernDialog.ShowMessage("Bład api. Status code: " + response.StatusCode + "\nReason phrase: " + response.ReasonPhrase, "Błąd", System.Windows.MessageBoxButton.OK);
                    return false;
                }
            }
        }

    }
}
