﻿using FilmNet.DataModel.DTO;
using FilmNet.Entities.Classes;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FilmNet.Tools.Client
{
    public static class RoleClient
    {
        public static HttpClient Initialize()
        {
            HttpClient client = Client.Initialize();
            client.BaseAddress = new Uri("https://microsoft-apiappd9f45e6e079044e1b6999233716ec27d.azurewebsites.net/api/roles/");
            return client;
        }
        
        public async static Task<IEnumerable<Role>> GetActorRoles(int actorid)
        {
            using (HttpClient client = Initialize())
            {
                HttpResponseMessage response = await client.GetAsync($"getactorroles?actorid={actorid}");
                if (response.IsSuccessStatusCode)
                {
                    return AutoMapper.Mapper.Map<IEnumerable<Role>>(await response.Content.ReadAsAsync<IEnumerable<FilmRoleDTO>>());
                }
                else
                {
                    ModernDialog.ShowMessage("Bład api. Status code: " + response.StatusCode + "\nReason phrase: " + response.ReasonPhrase, "Błąd", System.Windows.MessageBoxButton.OK);
                    return null;
                }
            }
        }

        public async static Task<IEnumerable<Role>> GetFilmRoles(int filmid)
        {
            HttpClient client = Initialize();
            HttpResponseMessage response = await client.GetAsync($"getfilmroles?filmid={filmid}");
            if (response.IsSuccessStatusCode)
            {
                return AutoMapper.Mapper.Map<IEnumerable<Role>>(await response.Content.ReadAsAsync<IEnumerable<FilmRoleDTO>>());
            }
            else
            {
                return null;
            }
        }
    }
}
