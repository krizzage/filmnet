﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace FilmNet.Tools.Client
{
    public static class Client
    {
        public static HttpClient client { get; set; }

        public static HttpClient Initialize()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://microsoft-apiappd9f45e6e079044e1b6999233716ec27d.azurewebsites.net/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }


    }
}
