﻿using FilmNet.DataModel;
using FilmNet.Entities.Classes;
using FilmNet.Tools;
using FilmNet.Tools.Client;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FilmNet.Pages.Main
{
    public partial class Films : UserControl
    {
        private PagingCollectionView films;

        public Films()
        {
            InitializeComponent();
            initFilms();
            initCombos();
        }

        private async void initCombos()
        {
            GenreFilterCombo.Items.Add("");
            SortCombo.Items.Add("");
            try
            {
                var genres = await GenreClient.GetGenres();
                foreach (var x in genres.ToList().Select(x => x.Name))
                    GenreFilterCombo.Items.Add(x);
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                dialog.ShowDialog();
                return;
            }
            foreach (var z in FilmsGrid.Columns.ToList().Select(x => x.Header))
                SortCombo.Items.Add(z);
        }

        private async void initFilms()
        {
            try
            {
                var init = await FilmsClient.GetFilms();
                films = new PagingCollectionView(init.ToList(), 10);
                RefreshDataGrid();
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                dialog.ShowDialog();
                return;
            }
        }

        private void OnNextClicked(object sender, RoutedEventArgs e)
        {
            this.films.MoveToNextPage();
        }

        private void OnPreviousClicked(object sender, RoutedEventArgs e)
        {
            this.films.MoveToPreviousPage();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshDataGrid();
        }

        private async void RefreshDataGrid()
        {
            try
            {
                var dataContext = await FilmsClient.GetFilms(SearchTextBox.Text, GenreFilterCombo.Text, SortCombo.Text, AscDescCheckBox.IsChecked.Value);
                films = new PagingCollectionView(dataContext.ToList(), 10);
                this.DataContext = films;
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                dialog.ShowDialog();
                return;
            }
        }
        private void GenreFilterCombo_DropDownClosed(object sender, EventArgs e)
        {
            RefreshDataGrid();
        }

        private void SortCombo_DropDownClosed(object sender, EventArgs e)
        {
            RefreshDataGrid();
        }

        private void AscDescCheckBox_Click(object sender, RoutedEventArgs e)
        {
            RefreshDataGrid();
        }

        private void AddFilm_Click(object sender, RoutedEventArgs e)
        {
            AddEditFilm newFilmDialog = new AddEditFilm(null);
            newFilmDialog.ShowDialog();
            RefreshDataGrid();
        }

        private async void Delete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Film toDelete = (Film)FilmsGrid.SelectedItem;
                await FilmsClient.DeleteFilm(toDelete.FilmId);
                RefreshDataGrid();
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                dialog.ShowDialog();
                return;
            }
        }

        private void FilmsGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var q = (Film)FilmsGrid.SelectedItem;
            if (q == null) return;

            var dlg = new ViewFilm(q);
            dlg.ShowDialog();
            RefreshDataGrid();
        }

        private void EditFilm_Click(object sender, RoutedEventArgs e)
        {
            var q = (Film)FilmsGrid.SelectedItem;
          
            AddEditFilm editFilmDialog = new AddEditFilm(q);
            editFilmDialog.ShowDialog();
            RefreshDataGrid();
        }
    }
}

