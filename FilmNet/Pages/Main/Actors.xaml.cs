﻿using FilmNet.DataModel;
using FilmNet.Entities.Classes;
using FilmNet.Pages.Actors;
using FilmNet.Tools;
using FilmNet.Tools.Client;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FilmNet.Pages.Main
{
    /// <summary>
    /// Interaction logic for Actors.xaml
    /// </summary>
    public partial class Actors : UserControl
    {
        private PagingCollectionView actors;

        public Actors()
        {
            InitializeComponent();
            initActors();
            initCombos();
        }

        private void initCombos()
        {
            SortCombo.Items.Add("");
            foreach (var z in ActorsGrid.Columns.ToList().Select(x => x.Header))
                SortCombo.Items.Add(z);
        }

        private async void initActors()
        {
            try
            {
                var dataContext = await ActorClient.GetActors();
                actors = new PagingCollectionView(dataContext.ToList(), 10);
                RefreshDataGrid();
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                dialog.ShowDialog();
                return;
            }

        }

        private void OnNextClicked(object sender, RoutedEventArgs e)
        {
            this.actors.MoveToNextPage();
        }

        private void OnPreviousClicked(object sender, RoutedEventArgs e)
        {
            this.actors.MoveToPreviousPage();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshDataGrid();
        }

        private async void RefreshDataGrid()
        {
            try
            {
                var dataContext = await ActorClient.GetActors(SearchTextBox.Text, SortCombo.Text, AscDescCheckBox.IsChecked.Value);
                actors = new PagingCollectionView(dataContext.ToList(), 10);
                this.DataContext = actors;
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                dialog.ShowDialog();
                return;
            }
        }
        private void GenreFilterCombo_DropDownClosed(object sender, EventArgs e)
        {
            RefreshDataGrid();
        }

        private void SortCombo_DropDownClosed(object sender, EventArgs e)
        {
            RefreshDataGrid();
        }

        private void AscDescCheckBox_Click(object sender, RoutedEventArgs e)
        {
            RefreshDataGrid();
        }

        private void AddActor_Click(object sender, RoutedEventArgs e)
        {
            AddEditActor dialog = new AddEditActor(null);
            dialog.ShowDialog();
            RefreshDataGrid();
        }

        private async void Delete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Actor toDelete = (Actor)ActorsGrid.SelectedItem;
                if (toDelete == null)
                    return;
                await ActorClient.DeleteActor(toDelete.ActorId);
                RefreshDataGrid();
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                dialog.ShowDialog();
                return;
            }
        }

        private void ActorsGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var q = (Actor)ActorsGrid.SelectedItem;
            if (q == null) return;

            var dlg = new ViewActor(q);
            dlg.Owner = Window.GetWindow(this);
            dlg.ShowDialog();
        }

        private void EditActor_Click(object sender, RoutedEventArgs e)
        {
            var q = (Actor)ActorsGrid.SelectedItem;
            if (q == null)
                return;

            AddEditActor newActorDialog = new AddEditActor(q);
            newActorDialog.Owner = Window.GetWindow(this);
            newActorDialog.ShowDialog();
            RefreshDataGrid();

        }
    }
}
