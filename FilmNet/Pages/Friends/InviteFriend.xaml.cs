﻿using FilmNet.DataModel;
using FilmNet.DataModel.DTO;
using FilmNet.Entities.Classes;
using FilmNet.Tools.Client;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FilmNet.Pages.Friends
{
    /// <summary>
    /// Interaction logic for InviteFriend.xaml
    /// </summary>
    public partial class InviteFriend : ModernWindow
    {

        public InviteFriend()
        {
            InitializeComponent();
        }

        private async void InviteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                User userToInvite = await UserClient.GetUserByLogin(LoginTextBox.Text);
                User from = await UserClient.GetUserById(UserInfo.UserId);
                IEnumerable<Friend> userFriends = await UserClient.GetUserFriends(UserInfo.UserId);
                IEnumerable<Invitation> userToInvitations = new List<Invitation>();
                IEnumerable<Invitation> userFromInvitations = await UserClient.GetUserInvitations(UserInfo.UserId);

                if (userToInvite != null)
                {
                    userToInvitations = await UserClient.GetUserInvitations(userToInvite.UserId);
                }
                if (userToInvite == null)
                {
                    ValidationMessage.Text = "Użytkownik nie istnieje";
                }
                else if (userFriends.SingleOrDefault(x => x.UserFriend.Login == LoginTextBox.Text) != null)
                {
                    ValidationMessage.Text = "Użytkownik jest już znajomym";
                }
                else if (userToInvitations.SingleOrDefault(x => x.InviterId == from.UserId) != null)
                {
                    ValidationMessage.Text = "Użytkownik jest już zaproszony";
                }
                else if (userFromInvitations.SingleOrDefault(x => x.InviterId == userToInvite.UserId) != null)
                {
                    ValidationMessage.Text = "Użytkownik już Cię zaprosił";
                }
                else if (userToInvite.Login == UserInfo.UserName)
                {
                    ValidationMessage.Text = "Nie możesz zaprosić siebie";
                }
                else
                {
                    await UserClient.AddInvitation(new InvitationDTO() { UserId = UserInfo.UserId, InviterId = userToInvite.UserId });
                    ModernDialog dialog = new ModernDialog() { Title = "Zaproszenie wysłane", Content = String.Format("Zaproszono użytkownika {0}", userToInvite.Login) };
                    dialog.ShowDialog();
                }
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                dialog.ShowDialog();
                return;
            }
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
