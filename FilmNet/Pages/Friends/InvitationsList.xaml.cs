﻿using FilmNet.DataModel;
using FilmNet.DataModel.DTO;
using FilmNet.Entities.Classes;
using FilmNet.Tools.Client;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FilmNet.Pages.Friends
{
    /// <summary>
    /// Interaction logic for InvitationsList.xaml
    /// </summary>
    public partial class InvitationsList : ModernWindow
    {
        public InvitationsList()
        {
            InitializeComponent();
            RefreshGrid();
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private async void AcceptInvitation_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Invitation accept = (Invitation)InvitationsGrid.SelectedItem;
                await UserClient.AcceptInvitation(new InvitationDTO() { UserId = accept.UserId, InviterId = accept.InviterId });
                RefreshGrid();
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                dialog.ShowDialog();
                return;
            }
        }

        private async void DeclineInvitation_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Invitation decline = (Invitation)InvitationsGrid.SelectedItem;
                await UserClient.DeclineInvitation(new InvitationDTO() { UserId = decline.UserId, InviterId = decline.InviterId });
                RefreshGrid();
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                dialog.ShowDialog();
                return;
            }
        }

        private async void RefreshGrid()
        {
            try
            {
                var list = await UserClient.GetUserInvitations(UserInfo.UserId);
                this.DataContext = list;
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                dialog.ShowDialog();
                return;
            }
        }

    }
}
