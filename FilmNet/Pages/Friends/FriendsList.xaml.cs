﻿using FilmNet.DataModel;
using FilmNet.DataModel.Repositories;
using FilmNet.Entities.Classes;
using FilmNet.Tools;
using FilmNet.Tools.Client;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FilmNet.Pages.Friends
{
    /// <summary>
    /// Interaction logic for FriendsList.xaml
    /// </summary>
    public partial class FriendsList
    {

        // TODO : Zmienić format daty wyświetlany w gridzie albo przechowywany w bazie żeby był dd mm rrrr obojetnie co pomiedzy moze byc / lub -
        private PagingCollectionView friends;

        public FriendsList()
        {
            InitializeComponent();
            InitializeFriends();
        }

        private async void InitializeFriends()
        {
            try
            {
                var dataContext = await UserClient.GetUserFriends(UserInfo.UserId);
                friends = new PagingCollectionView(dataContext.ToList(), 5);
                this.DataContext = friends;
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                dialog.ShowDialog();
                return;
            }
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private async void FriendsGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var q = (Friend)FriendsGrid.SelectedItem;
                if (q == null) return;

                var user = await UserClient.GetUserById(q.UserFriendId);
                var dlg = new FriendProfile(user);
                dlg.ShowDialog();
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                dialog.ShowDialog();
                return;
            }
        }

        private void OnNextClicked(object sender, RoutedEventArgs e)
        {
            if (friends != null)
                this.friends.MoveToNextPage();
        }

        private void OnPreviousClicked(object sender, RoutedEventArgs e)
        {
            if(friends != null)
                this.friends.MoveToPreviousPage();
        }
    }
}
