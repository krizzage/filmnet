﻿using FilmNet.DataModel;
using FilmNet.Entities.Classes;
using FilmNet.Tools.Client;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FilmNet.Pages.Friends
{
    /// <summary>
    /// Interaction logic for FriendProfile.xaml
    /// </summary>
    public partial class FriendProfile : ModernWindow
    {
        public User Friend { get; set; }

        public FriendProfile(User _friend)
        {
            InitializeComponent();
            Friend = _friend;
            FriendTextBox.Text = Friend.Login;
            InitGrid();
        }

        private async void InitGrid()
        {
            try
            {
                var ratings = await UserClient.GetUserRatings(Friend.UserId);
                this.DataContext = ratings.ToList();
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                dialog.ShowDialog();
                return;
            }
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
