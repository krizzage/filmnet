﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FilmNet.Pages.Friends
{
    /// <summary>
    /// Interaction logic for Friends.xaml
    /// </summary>
    public partial class Friends : UserControl
    {
        public Friends()
        {
            InitializeComponent();
        }

        private void FriendsBtn_Click(object sender, RoutedEventArgs e)
        {
            FriendsList friendsList = new FriendsList();
            friendsList.ShowDialog();
        }

        private void InviteFriend_Click(object sender, RoutedEventArgs e)
        {
            InviteFriend inviteFriend = new InviteFriend();
            inviteFriend.ShowDialog();
        }

        private void AcceptInvitation_Click(object sender, RoutedEventArgs e)
        {
            InvitationsList invitations = new InvitationsList();
            invitations.ShowDialog();
        }
    }
}
