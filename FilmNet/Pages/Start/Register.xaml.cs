﻿using FilmNet.DataModel;
using FilmNet.Entities.Classes;
using FilmNet.Tools.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FilmNet.Pages
{
    /// <summary>
    /// Interaction logic for Register.xaml
    /// </summary>
    public partial class Register : UserControl
    {
        RegisterValidator validator; 

        public Register()
        {
            InitializeComponent();
            validator = new RegisterValidator(this);
        }

        private async void RegisterButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                User user = new User() { Login = LoginTextBox.Text, Password = PasswordTextBox.Password };
                await UserClient.AddUser(user);
                user = await UserClient.GetUserByLogin(user.Login);
                UserInfo.InitUser(user);

                MainWindow mw = new MainWindow();
                mw.Show();

                var wnd = Window.GetWindow(this);
                wnd.Close();
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ValidationTextBox.Text = "Błąd sieci, sprawdź połączenie";
                return;
            }
        }

        private async void LoginTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
                await validator.ValidateLogin();
        }


        private void PasswordChanged(object sender, RoutedEventArgs e)
        {
            validator.ValidatePassword();
        }



    }
}
