﻿using FilmNet.DataModel;
using FilmNet.Entities.Classes;
using FilmNet.Tools.Client;
using System.Windows;
using System.Windows.Controls;
using System;
using System.Threading.Tasks;
using FilmNet.DataModel.DTO;
using FirstFloor.ModernUI.Windows.Controls;

namespace FilmNet.Pages
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : UserControl
    {

        public Login()
        {
            InitializeComponent();
        }

        private async void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            User tmp = null;
            try
            {
                tmp = await TryLogin();
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ValidationTextBox.Text = "Błąd sieci, sprawdź połączenie";
                return;
            }

            if(tmp == null)
                ValidationTextBox.Text = "Użytkownik o podanym loginie i haśle nie istnieje";
            else
            {
                UserInfo.InitUser(tmp);

                MainWindow mw = new MainWindow();
                mw.Show();

                var wnd = Window.GetWindow(this);
                wnd.Close();
            }
        }

        private async Task<User> TryLogin()
        {
            User tmp = await UserClient.Login(LoginTextBox.Text, PasswordTextBox.Password);
            return tmp;
        }
    }
}
