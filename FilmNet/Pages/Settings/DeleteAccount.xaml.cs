﻿using FilmNet.DataModel;
using FilmNet.Entities.Classes;
using FilmNet.Tools.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FilmNet.Pages.Settings
{
    public partial class DeleteAccount : UserControl
    {
        public DeleteAccount()
        {
            InitializeComponent();
        }

        private async void DeleteAccountButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Password.Password == UserInfo.UserPassword)
                {
                    await UserClient.DeleteUser(UserInfo.UserId);
                    ShowLoginWindow();
                }
                else
                    ValidationTextBox.Text = "Niepoprawne hasło";
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ValidationTextBox.Text = "Błąd sieci, sprawdź połączenie";
                return;
            }
        }

        private void ShowLoginWindow()
        {
            StartWindow sw = new StartWindow();
            sw.Show();

            var wnd = Window.GetWindow(this);
            wnd.Close();
        }
    }
}
