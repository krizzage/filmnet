﻿using FilmNet.DataModel;
using FilmNet.Entities.Classes;
using FilmNet.Tools.Client;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FilmNet.Pages.Settings
{
    /// <summary>
    /// Interaction logic for ChangePassword.xaml
    /// </summary>
    public partial class ChangePassword : UserControl
    {
        PasswordValidator validator;

        public ChangePassword()
        {
            InitializeComponent();
            validator = new PasswordValidator(NewPassword, ConfirmNewPassword, ValidationTextBox);
        }

        private async void ChangePasswordButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (PasswordTextBox.Password != UserInfo.UserPassword)
                    ValidationTextBox.Text = "Błędne hasło użytkownika";
                else if (validator.Validate())
                {
                    User user = await UserClient.GetUserById(UserInfo.UserId);
                    user.Password = NewPassword.Password;
                    UserInfo.UserPassword = NewPassword.Password;
                    ModernDialog.ShowMessage("Hasło zostało zmienione!", "Hasło zmienione", MessageBoxButton.OK, Window.GetWindow(this));
                    GoBack();
                }
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ValidationTextBox.Text = "Błąd sieci, sprawdź połączenie";
                return;
            }
        }

        private void GoBack()
        {
            BBCodeBlock bs = new BBCodeBlock();
            try
            {
                bs.LinkNavigator.Navigate(new Uri("/Pages/Settings/Settings.xaml", UriKind.Relative), this);
            }
            catch (Exception error)
            {
                ModernDialog.ShowMessage(error.Message, FirstFloor.ModernUI.Resources.NavigationFailed, MessageBoxButton.OK);
            }
        }

    }
}
