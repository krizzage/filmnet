﻿using FilmNet.DataModel;
using FilmNet.Entities.Classes;
using FilmNet.Tools.Client;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FilmNet.Pages.Actors
{
    /// <summary>
    /// Interaction logic for AddEditActor.xaml
    /// </summary>
    public partial class AddEditActor : ModernWindow
    {
        private Actor toUpdate;
        private Actor dataContext;

        public AddEditActor(Actor upd)
        {
           InitializeComponent();
            dataContext = new Actor();
            if (upd != null)
            {
                toUpdate = upd;
                Header.Text = "Edytuj aktora";
                InitActor(dataContext, toUpdate);
            }
            else
                Header.Text = "Dodaj aktora";

            this.DataContext = dataContext;
        }

        private void InitActor(Actor dest, Actor src)
        {
            dest.Name = src.Name;
            dest.Surname = src.Surname;
            dest.Description = src.Description;
        }

        private async void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (await SaveActor())
                    this.Close();
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                dialog.ShowDialog();
                return;
            }
        }

        private async Task<bool> SaveActor()
        {
            bool response = false;
            try
            {
                if (toUpdate != null)
                {
                    InitActor(toUpdate, dataContext);
                    response = await ActorClient.UpdateActor(toUpdate);
                }
                else
                {
                    response = await ActorClient.AddActor(dataContext);
                }

                return response;
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                dialog.ShowDialog();
                return false;
            }
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            CloseWindow();
        }

        private void CloseWindow()
        {
            var wnd = Window.GetWindow(this);
            wnd.Close();
        }
    }
}
