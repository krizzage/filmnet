﻿using FilmNet.Entities.Classes;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FilmNet.Pages.Actors
{
    /// <summary>
    /// Interaction logic for ViewActor.xaml
    /// </summary>
    public partial class ViewActor : ModernWindow
    {
        public ViewActor(Actor actor)
        {
            this.DataContext = actor;
            InitializeComponent();
        }
        

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            var wnd = Window.GetWindow(this);
            wnd.Close();
        }
    }
}
