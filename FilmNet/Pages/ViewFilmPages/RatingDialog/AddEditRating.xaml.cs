﻿using FilmNet.DataModel;
using FilmNet.Entities.Classes;
using FilmNet.Tools;
using FilmNet.Tools.Client;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FilmNet.Pages.ViewFilmPages.RatingDialog
{
    /// <summary>
    /// Interaction logic for AddEditRating.xaml
    /// </summary>
    public partial class AddEditRating : ModernDialog
    {
        private Rating userRating;
        private bool addingState;

        public AddEditRating()
        {
            InitializeComponent();
            InitUserRating();
            RatingComboBox.ItemsSource = new List<int>() { 1, 2, 3, 4, 5 };
            // define the dialog buttons
            this.Buttons = new Button[] { };

        }

        private async void InitUserRating()
        {
            try
            {
                userRating = await RatingClient.GetRating(UserInfo.UserId, ViewFilm.film.FilmId);
                if (userRating == null)
                {
                    userRating = new Rating();
                    addingState = true;
                    RatingComboBox.SelectedValue = 1;
                }
                this.DataContext = userRating;
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                dialog.ShowDialog();
                return;
            }
        }

        private async void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                await SaveComment();
                this.Close();
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                dialog.ShowDialog();
                return;
            }
        }

        private async Task SaveComment()
        {
            try
            {
                userRating.Comment = CommentTextBox.Text;
                userRating.UserRating = (int)RatingComboBox.SelectedItem;
                if (addingState)
                {
                    userRating.FilmId = ViewFilm.film.FilmId;
                    userRating.UserId = UserInfo.UserId;
                    await RatingClient.AddRating(userRating);
                }
                else
                    await RatingClient.UpdateRating(userRating);
                await UpdateFilmAverage();
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                dialog.ShowDialog();
                return;
            }
        }

        private async Task UpdateFilmAverage()
        {
            try
            {
                var film = await FilmsClient.GetFilmById(ViewFilm.film.FilmId);
                var ratings = await RatingClient.GetRatingsByFilmId(ViewFilm.film.FilmId);
                film.Average = Math.Round(ratings.Average(x => x.UserRating));
                await FilmsClient.UpdateFilm(film);
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                dialog.ShowDialog();
                return;
            }
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;

            this.Close();
        }
    }
}
