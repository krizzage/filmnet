﻿using FilmNet.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FilmNet.Pages.ViewFilmPages
{
    /// <summary>
    /// Interaction logic for FilmInfo.xaml
    /// </summary>
    public partial class FilmInfo : UserControl
    {
        public FilmInfo()
        {
            this.DataContext = ViewFilm.film;
            InitializeComponent();
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            var wnd = Window.GetWindow(this);
            wnd.Close();
        }
    }
}
