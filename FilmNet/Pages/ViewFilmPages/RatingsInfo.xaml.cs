﻿using FilmNet.DataModel;
using FilmNet.Pages.ViewFilmPages.RatingDialog;
using FilmNet.Tools;
using FilmNet.Tools.Client;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FilmNet.Pages.ViewFilmPages
{
    /// <summary>
    /// Interaction logic for RatingsInfo.xaml
    /// </summary>
    public partial class RatingsInfo : UserControl
    {
        private PagingCollectionView ratings;

        public RatingsInfo()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            RefreshCommentGrid();
        }

        private async void RefreshCommentGrid()
        {
            try
            {
                var dataContext = await RatingClient.GetRatingsByFilmId(ViewFilm.film.FilmId);
                ratings = new PagingCollectionView(dataContext.ToList(), 10);
                RatingsGrid.DataContext = ratings;
                CommentButton.Content = await RatingClient.GetRating(UserInfo.UserId, ViewFilm.film.FilmId) == null ? "Dodaj ocenę" : "Edytuj ocenę";
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                dialog.ShowDialog();
                return;
            }
        }

        private void OnNextClicked(object sender, RoutedEventArgs e)
        {
            this.ratings.MoveToNextPage();
        }

        private void OnPreviousClicked(object sender, RoutedEventArgs e)
        {
            this.ratings.MoveToPreviousPage();
        }

        private void CommentButton_Click(object sender, RoutedEventArgs e)
        {
            AddEditRating comment = new AddEditRating();
            comment.ShowDialog();
            RefreshCommentGrid();
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            var wnd = Window.GetWindow(this);
            wnd.Close();
        }
    }
}
