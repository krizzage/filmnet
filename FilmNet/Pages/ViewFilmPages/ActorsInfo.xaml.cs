﻿using FilmNet.DataModel;
using FilmNet.Entities.Classes;
using FilmNet.Tools;
using FilmNet.Tools.Client;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FilmNet.Pages.ViewFilmPages
{
    /// <summary>
    /// Interaction logic for ActorsInfo.xaml
    /// </summary>
    public partial class ActorsInfo : UserControl
    {
        private PagingCollectionView roles;

        public ActorsInfo()
        {
            roles = new PagingCollectionView(new List<Role>(), 10);
            InitializeComponent();
            InitActorGrid();
        }

        private async void InitActorGrid()
        {
            try
            {
                var r = await RoleClient.GetFilmRoles(ViewFilm.film.FilmId);
                if (r != null)
                {
                    roles = new PagingCollectionView(r.ToList(), 10);
                    RolesGrid.DataContext = roles;
                }
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                dialog.ShowDialog();
                return;
            }
        }

        private void OnNextClicked(object sender, RoutedEventArgs e)
        {
            this.roles.MoveToNextPage();
        }

        private void OnPreviousClicked(object sender, RoutedEventArgs e)
        {
            this.roles.MoveToPreviousPage();
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            var wnd = Window.GetWindow(this);
            wnd.Close();
        }
    }
}
