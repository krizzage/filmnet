﻿using FilmNet.DataModel;
using FilmNet.Entities.Classes;
using FilmNet.Tools;
using FilmNet.Tools.Client;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FilmNet.Pages.AddEditFilmPages
{
    /// <summary>
    /// Interaction logic for AddRole.xaml
    /// </summary>
    public partial class AddRole : ModernDialog
    {
        private PagingCollectionView actors;
        private ICollection<Role> roles;

        public AddRole(ICollection<Role> roles)
        {
            InitializeComponent();
            this.roles = roles;
            initActors();
            this.Buttons = new Button[] { };
        }

        private async void initActors()
        {
            try
            {
                var dataContext = await ActorClient.GetActors();
                actors = new PagingCollectionView(dataContext.ToList(), 5);
                RefreshDataGrid();
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                dialog.ShowDialog();
                return;
            }
        }

        private void OnNextClicked(object sender, RoutedEventArgs e)
        {
            this.actors.MoveToNextPage();
        }

        private void OnPreviousClicked(object sender, RoutedEventArgs e)
        {
            this.actors.MoveToPreviousPage();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshDataGrid();
        }

        private async void RefreshDataGrid()
        {
            try
            {
                var dataContext = await ActorClient.GetActors(SearchTextBox.Text);
                actors = new PagingCollectionView(dataContext.ToList(), 5);
                this.DataContext = actors;
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                dialog.ShowDialog();
                return;
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            Actor actor = (Actor)ActorsGrid.SelectedItem;
            if (actor == null)
                this.Close();

            Role change = roles.FirstOrDefault(x => x.ActorId == actor.ActorId && x.FilmId == AddEditFilm.film.FilmId);
            if (change != null)
                change.Name = RoleTextBox.Text;
            else
                roles.Add(new Role() { Actor = actor, ActorId = actor.ActorId, FilmId = AddEditFilm.film.FilmId, Name = RoleTextBox.Text });

            this.DialogResult = true;
            this.Close();
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;

            this.Close();
        }
        

    }
}
