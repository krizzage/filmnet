﻿using FilmNet.DataModel;
using FilmNet.Entities.Classes;
using FilmNet.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FirstFloor.ModernUI.Windows.Controls;

namespace FilmNet.Pages.AddEditFilmPages
{
    /// <summary>
    /// Interaction logic for ActorsInfoAddEdit.xaml
    /// </summary>
    public partial class ActorsInfoAddEdit : UserControl
    {
        private PagingCollectionView roles;
        private Film dataContext;

        public ActorsInfoAddEdit()
        {
            dataContext = AddEditFilm.film;
            RefreshDataGrid();
            InitializeComponent();
        }

        private void OnNextClicked(object sender, RoutedEventArgs e)
        {
            this.roles.MoveToNextPage();
        }

        private void OnPreviousClicked(object sender, RoutedEventArgs e)
        {
            this.roles.MoveToPreviousPage();
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            Role toDelete = (Role)RolesGrid.SelectedItem;
            if (toDelete == null)
                return;
            dataContext.Roles.Remove(toDelete);
            RefreshDataGrid();
        }

        private void RefreshDataGrid()
        {
            roles = new PagingCollectionView(dataContext.Roles.ToList(), 10);
            this.DataContext = roles;
        }

        private async void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (await AddEditFilm.SaveFilm())
                    CloseWindow();
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                dialog.ShowDialog();
                return;
            }
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            CloseWindow();
        }

        private void CloseWindow()
        {
            var wnd = Window.GetWindow(this);
            wnd.Close();
        }

        private void AddFilm_Click(object sender, RoutedEventArgs e)
        {
            AddRole role = new AddRole(dataContext.Roles);
            if (role.ShowDialog() == true)
                RefreshDataGrid();
        }
    }
}
