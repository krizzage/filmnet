﻿using FilmNet.DataModel;
using FilmNet.Entities.Classes;
using FirstFloor.ModernUI.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FirstFloor.ModernUI.Windows.Navigation;
using FilmNet.Tools.Client;
using FirstFloor.ModernUI.Windows.Controls;

namespace FilmNet.Pages.AddEditFilmPages
{
    /// <summary>
    /// Interaction logic for FilmInfoAddEdit.xaml
    /// </summary>
    public partial class FilmInfoAddEdit : UserControl
    {
        private Film dataContext;

        public FilmInfoAddEdit()
        {
            InitializeComponent();
            dataContext = AddEditFilm.film;
            InitGenreComboBox();
            this.DataContext = dataContext;
        }

        private async void InitGenreComboBox()
        {
            try
            {
                var genres = await GenreClient.GetGenres();
                GenreComboBox.ItemsSource = genres.Select(x => x.Name).ToList();
                if (AddEditFilm.State == States.Modyfing)
                {
                    GenreComboBox.SelectedValue = AddEditFilm.toUpdate.Genre.Name;
                }
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                dialog.ShowDialog();
                return;
            }
        }

        private async void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (await AddEditFilm.SaveFilm())
                    CloseWindow();
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                dialog.ShowDialog();
                return;
            }
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            CloseWindow();
        }

        private void CloseWindow()
        {
            var wnd = Window.GetWindow(this);
            wnd.Close();
        }

        private async void GenreComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var genre = await GenreClient.GetGenreByName((sender as ComboBox).SelectedItem as string);
                if (genre != null)
                    dataContext.GenreId = genre.GenreId;
            }
            catch (System.Net.Http.HttpRequestException)
            {
                ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                dialog.ShowDialog();
                return;
            }
        }
    }
}
