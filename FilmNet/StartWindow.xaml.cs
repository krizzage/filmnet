﻿using FilmNet.DataModel;
using FilmNet.Entities.Classes;
using FilmNet.Tools;
using FilmNet.Tools.Client;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace FilmNet
{
    /// <summary>
    /// Interaction logic for StartWindow.xaml
    /// </summary>
    public partial class StartWindow
    {
        public StartWindow()
        {
            InitializeComponent();
            AutoMapperConfig.RegisterMappings();

            var tmp = new FilmNetContext();
            tmp.Database.Initialize(false);
        }
        
    }
}
