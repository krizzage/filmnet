﻿using FilmNet.DataModel;
using FilmNet.Entities.Classes;
using FilmNet.Tools.Client;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FilmNet
{
    /// <summary>
    /// Interaction logic for AddEditFilm.xaml
    /// </summary>
    public enum States { Adding, Modyfing };

    public partial class AddEditFilm
    {
        public static Film film;
        public static Film toUpdate;
        public static States State;

        public AddEditFilm(Film f)
        {
            toUpdate = f;
            film = new Film();
            if (f == null)
            {
                State = States.Adding;
            }
            else
            {
                State = States.Modyfing;
                InitFilm(film, f);
            }

            InitializeComponent();
        }

        private async static Task InitFilm(Film dest, Film source)
        {
            dest.Name = source.Name;
            dest.Description = source.Description;
            dest.Average = source.Average;
            dest.FilmId = source.FilmId;
            dest.Year = source.Year;
            dest.GenreId = source.GenreId;
            try
            {
                dest.Genre = await GenreClient.GetGenreById(source.GenreId);

            }
            catch (System.Net.Http.HttpRequestException)
            {
                ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                dialog.ShowDialog();
                return;
            }
            dest.Roles.Clear();
            foreach (var r in source.Roles)
                dest.Roles.Add(r);
        }

        public async static Task<bool> SaveFilm()
        {
            bool response = false;
            if (AddEditFilm.Validate())
            {
                try
                {
                    if (AddEditFilm.State == States.Modyfing)
                    {
                        // await UpdateRoles();
                        await InitFilm(toUpdate, film);
                        response = await FilmsClient.UpdateFilm(toUpdate);
                    }
                    else
                    {
                        response = await FilmsClient.AddFilm(film);
                    }
                }
                catch (System.Net.Http.HttpRequestException)
                {
                    ModernDialog dialog = new ModernDialog() { Title = "Błąd sieci", Content = "Sprawdź połączenie sieciowe" };
                    dialog.ShowDialog();
                    return false;
                }
            }
            return response;
        }
        
        private static bool Validate()
        {
            if(AddEditFilm.film.GenreId == 0)
            {
                ModernDialog.ShowMessage("Gatunek filmu nie został wypełniony", "Wypełnij gatunek", System.Windows.MessageBoxButton.OK);
                return false;
            }
            return true;
        }
    }

}

