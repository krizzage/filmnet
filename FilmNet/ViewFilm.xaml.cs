﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using FilmNet.Entities.Classes;
using FilmNet.DataModel;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Navigation;

namespace FilmNet
{
    public partial class ViewFilm
    {
        public static Film film;
        
        public ViewFilm(Film q)
        {
            film = q;
            InitializeComponent();
        }
    }
}
